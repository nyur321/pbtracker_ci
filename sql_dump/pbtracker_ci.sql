-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2014 at 10:28 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pbtracker_ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblclinicaindata`
--

CREATE TABLE IF NOT EXISTS `tblclinicaindata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `new_cases` int(11) NOT NULL,
  `active_discharges` int(11) NOT NULL,
  `passive_discharges` int(11) NOT NULL,
  `added_cases` int(11) NOT NULL,
  `removed_cases` int(11) NOT NULL,
  `active_cases` int(11) NOT NULL,
  `visits_attended` int(11) NOT NULL,
  `no_show` int(11) NOT NULL,
  `units_charged` float NOT NULL,
  `days_worked` int(11) NOT NULL,
  `hours_worked` float NOT NULL,
  `hours_scheduled` float NOT NULL,
  `charges` float NOT NULL,
  `billing_days` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblclosedweeklydata`
--

CREATE TABLE IF NOT EXISTS `tblclosedweeklydata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblcollectionrate`
--

CREATE TABLE IF NOT EXISTS `tblcollectionrate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `collection_rate` float NOT NULL,
  `effective_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblinitialcases`
--

CREATE TABLE IF NOT EXISTS `tblinitialcases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `initial_cases` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbllocation`
--

CREATE TABLE IF NOT EXISTS `tbllocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `practice_id` int(11) NOT NULL,
  `location_code` varchar(128) NOT NULL,
  `location_name` varchar(128) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `city` varchar(128) NOT NULL,
  `state` varchar(128) NOT NULL,
  `zipcode` int(10) NOT NULL,
  `is_main` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `close_date` date NOT NULL,
  `location_status` tinyint(4) NOT NULL,
  `reporting_mode` tinyint(4) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblposition`
--

CREATE TABLE IF NOT EXISTS `tblposition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_code` varchar(128) NOT NULL,
  `position_name` varchar(128) NOT NULL,
  `practice_id` int(11) NOT NULL,
  `is_clinician` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tblposition`
--

INSERT INTO `tblposition` (`id`, `position_code`, `position_name`, `practice_id`, `is_clinician`) VALUES
(1, 'PT', 'Physical Therapist', 0, 1),
(2, 'PTA', 'Assistant Physical Therapist', 0, 1),
(3, 'NA', 'Non-Clinician', 0, 0),
(4, 'A', 'Aide', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblpractice`
--

CREATE TABLE IF NOT EXISTS `tblpractice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `practice_code` varchar(128) NOT NULL,
  `practice_name` varchar(128) NOT NULL,
  `practice_status` tinyint(4) NOT NULL,
  `logo` varchar(128) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tblrole`
--

CREATE TABLE IF NOT EXISTS `tblrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tblrole`
--

INSERT INTO `tblrole` (`id`, `role_name`) VALUES
(1, 'Super Administrator'),
(2, 'Practice Owner'),
(3, 'Office Administrator'),
(4, 'Clinician'),
(5, 'Biller'),
(6, 'Aide');

-- --------------------------------------------------------

--
-- Table structure for table `tbluserlocation`
--

CREATE TABLE IF NOT EXISTS `tbluserlocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `work_status` tinyint(4) NOT NULL,
  `start_date` date NOT NULL,
  `termination_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbluserposition`
--

CREATE TABLE IF NOT EXISTS `tbluserposition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbluserposition`
--

INSERT INTO `tbluserposition` (`id`, `user_id`, `position_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbluserpractice`
--

CREATE TABLE IF NOT EXISTS `tbluserpractice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `practice_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbluserrole`
--

CREATE TABLE IF NOT EXISTS `tbluserrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbluserrole`
--

INSERT INTO `tbluserrole` (`id`, `user_id`, `role_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE IF NOT EXISTS `tblusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(128) NOT NULL,
  `first_name` varchar(128) NOT NULL,
  `last_name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `contact_no` int(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `password_reset_date` date NOT NULL,
  `employment_status` tinyint(4) NOT NULL,
  `validation_dode` varchar(128) NOT NULL,
  `activation_status` tinyint(4) NOT NULL,
  `termination_date` date DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`id`, `employee_id`, `first_name`, `last_name`, `email`, `contact_no`, `password`, `password_reset_date`, `employment_status`, `validation_dode`, `activation_status`, `termination_date`, `timestamp`) VALUES
(1, '1', 'admin', 'admin', 'admin@email.com', 0, '4d4098d64e163d2726959455d046fd7c', '2020-01-01', 1, '123456abcdef', 1, '0000-00-00', '2014-05-05 09:56:34');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
