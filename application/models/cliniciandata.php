<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cliniciandata extends Table {

    public function get_clinician_data($user_id, $location_data, $start_date, $end_date) {

        $query = $this->db->query('SELECT * FROM tblcliniciandata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_data . '
AND start_date = "' . $start_date . '" 
AND end_date = "' . $end_date . '"' . ' ORDER BY start_date ASC' );
        return $query->result_array();
    }

    public function get_clinician_data_by_date($user_id, $location_id, $start_date, $end_date) {

        $query = $this->db->query('SELECT * FROM tblcliniciandata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_id . '
AND start_date >= "' . $start_date . '" 
AND start_date <= "' . $end_date . '"'.' ORDER BY start_date ASC');
//        $query = $this->db->query('SELECT * FROM tblcliniciandata
//WHERE user_id =  12
//AND location_id = 8
//AND start_date >= "2014-05-06" 
//AND start_date <= "2014-05-28"');
        return $query->result_array();
    }

    public function get_all_clinician_data_by_date($location_id, $user_id, $start_date, $end_date) {

        $query = $this->db->query('SELECT * FROM tblcliniciandata AS t1 
JOIN tblusers AS t2 ON t1.user_id = t2.id 
WHERE location_id = ' . $location_id . ' 
AND t1.user_id = ' . $user_id . ' 
AND start_date >= "' . $start_date . '" 
AND start_date <= "' . $end_date . '"' . ' ORDER BY t1.start_date ASC');
//        $query = $this->db->query('SELECT * FROM tblcliniciandata
//WHERE user_id =  12
//AND location_id = 8
//AND start_date >= "2014-05-06" 
//AND start_date <= "2014-05-28"');
        return $query->result_array();
    }

    public function create_clinician_data($data) {
        $result = $this->insert('tblcliniciandata', $data);
        return $result;
    }

    public function update_clinician_data($data, $user_id, $location_id, $start_date, $end_date) {
        $where = array(
            'user_id' => $user_id,
            'location_id' => $location_id,
            'start_date' => "'" . $start_date . "'",
            'end_date' => "'" . $end_date . "'"
        );
        $result = $this->update('tblcliniciandata', $data, $where);
        if ($this->db->affected_rows() != 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_clinician_data($user_id, $location_id, $start_date, $end_date) {
        $where = array(
            'user_id' => $user_id,
            'location_id' => $location_id,
            'start_date' => "'" . $start_date . "'",
            'end_date' => "'" . $end_date . "'"
        );
        $result = $this->delete('tblcliniciandata', $where);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function check_previous_active_case($date, $user_id, $location_id) {
        $query = $this->db->query('SELECT * FROM tblcliniciandata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_id . '
AND start_date < "' . $date . '" 
AND end_date < "' . $date . '"' . ' ORDER BY start_date DESC limit 1');
        return $query->result_array();
    }

    public function get_all_dates_forth($user_id, $location_id, $start_date, $end_date) {
        $query = $this->db->query('SELECT * FROM tblcliniciandata
WHERE user_id = ' . $user_id . ' AND location_id = ' . $location_id . '
AND start_date > ' . $start_date);
        return $query->result_array();
    }

//    public function check_if_update() {
//        $query = $this->db->query('SELECT * FROM tblcliniciandata
//WHERE user_id = ' . $user_id . ' 
//AND location_id = ' . $location_data . '
//AND start_date = "' . $start_date . '" 
//AND end_date = "' . $end_date . '"');
//        return $query->result_array();
//    }
}
