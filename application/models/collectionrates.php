<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Collectionrates extends Table {

    public function save_collectionrate($data) {
        $result = $this->insert('tblcollectionrate', $data);
        return $result;
    }

    public function get_locations_in_practice($lid) {
        $this->select();
        $this->from('tblcollectionrate');
        $this->where('practice_id=' . "'" . $lid . "'");
        $query = $this->get();
        return $query;
    }

    public function get_locations_in_closed($pid) {
        $this->select();
        $this->from('tbllocation AS l2');
        $this->join('tblcollectionrate AS c2', 'l2.id=c2.location_id');
        $this->where('l2.practice_id=' . "'" . $pid . "'" . " AND l2.location_status = 0");
        $query = $this->get();
        return $query;
    }

    public function get_all_locations_in_closed() {
        $this->select();
        $this->from('tbllocation AS l');
        $this->join('tblcollectionrate AS c', 'l.id=c.location_id');
        $this->where('location_status = 0');
        $query = $this->get();
        return $query;
    }

    public function create_collection_rate() {
        
    }

    public function update_collection($data, $locid) {
        $where = "location_id = " . $locid;
//        $result = $this->insert('tblcollectionrate', $data);
        $result = $this->update('tblcollectionrate', $data, $where);
        return $result;
    }

    public function get_current_collection_rate($locid) {
        $this->select();
        $this->from('tblcollectionrate');
        $this->where('location_id =' . $locid);
        $query = $this->get();
        return $query;
    }

    public function get_locations_in_closed_by_user($user_id) {
        $query = $this->db->query('SELECT * FROM tbllocation 
INNER JOIN tbluserlocation ON tbllocation.id = tbluserlocation.location_id 
WHERE tbllocation.location_status = 0 AND tbluserlocation.user_id =' . $user_id);
        return $query->result_array();
    }

//    public function get_collection_rate_by_date($location_id, $date) {
//
//        $this->select();
//        $this->from('tblcollectionratehistory');
//        $this->where('location_id = ' . $location_id . ' AND effective_date <= "' . $date . '"');
//        $this->orderby('id', 'DESC');
//        $this->limit(1);
//        $query = $this->get();
//        if ($query) {
//            $result = $query[0]['collection_rate'];
//        } else {
//            $this->select();
//            $this->from('tblcollectionrate');
//            $this->where('location_id = ' . $location_id);
//            $query = $this->get();
//            if ($query) {
//                $result = $query[0]['collection_rate'];
//            } else {
//                $result = 0;
//            }
//        }
//        return $result;
//    }

    public function get_collection_rate_by_date($location_id, $date) {

        $this->select();
        $this->from('tblcollectionrate');
        $this->where('location_id = ' . $location_id . ' AND effective_date <= "' . $date . '"');
        $query = $this->get();
        if ($query) {
            $result = $query[0]['collection_rate'];
        } else {
            $this->select();
            $this->from('tblcollectionratehistory');
            $this->where('location_id = ' . $location_id . ' AND effective_date <= "' . $date . '"');
            $this->orderby('effective_date', 'DESC');
            $this->limit(1);
            $query = $this->get();
            if ($query) {
                $result = $query[0]['collection_rate'];
            } else {
                $result = 0;
            }
        }
        return $result;
    }
    

}
