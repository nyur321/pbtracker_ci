<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Practices extends Table {

    function __construct() {
        parent::__construct();
    }

    public function get_all_practice() {
        $this->select();
        $this->from('tblpractice');
        $this->where('id > 1');
        $query = $this->get();
        return $query;
    }

    public function get_practice_by_userid($userID) {
        $this->select();
        $this->from('tblpractice');
        $this->where('user_id = ' . "'" . $userID . "'");
        $query = $this->get();
        return $query;
    }
    
    public function get_practice_by_id($id) {
        $this->select();
        $this->from('tblpractice');
        $this->where('id = ' . "'" . $id . "'");
        $query = $this->get();
        return $query;
    }

    public function get_practice_by_name($pName) {
        $this->select();
        $this->from('tblpractice');
        $this->where('practice_name = ' . "'" . $pName . "'");
        $query = $this->get();
        return $query;
    }

    public function get_practice_by_code($pCode) {
        $this->select();
        $this->from('tblpractice');
        $this->where('practice_code = ' . "'" . $pCode . "'");
        $query = $this->get();
        return $query;
    }

    public function save_practice($data) {
        $result = $this->insert('tblpractice', $data);
        return $result;
    }

    public function update_practice($data, $practiceId) {
        $result = $this->update('tblpractice', $data, 'id =' . $practiceId);
        return $result;
    }

}
