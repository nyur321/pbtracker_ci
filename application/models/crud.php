<?php
//A generic model that is extended by other models. All of its functions is available in the said models
class Crud extends CI_Model {
   //$name is the name of table in your db
    protected $name;
    //$key is the primary of your table
    protected $key = 'ID';
    //$orderby determines the order of your query.
    protected $orderby = '';

    public function __construct($name = "", $key = "") {
        parent::__construct();
        $this->name = $name;
        if ($key != "") {
            $this->key = $key;
        }
    }

    //Get all rows. Returns an array
    public function getAll() {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $query = $this->db->get($this->name);
        return $query->result_array();
    }

    //Get a specific row. Accepts an integer/id
    public function getRow($id) {
        $query = $this->db->get_where($this->name, array($this->key => $id));
        return $query->row_array();
    }

    //Get all rows with a specific column and value. Accepts column name and the value 
    public function getAllBy($column, $value) {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $this->db->where($column, $value);
        $query = $this->db->get($this->name);
        return $query->result_array();
    }
    
    //Get all rows with a where condition. where is an array of values and column names
    public function getAllByWhere($where) {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $this->db->where($where);
        $query = $this->db->get($this->name);
        return $query->result_array();
    }

    //get one row with column name and value. returns an array
    public function getOneBy($column, $value) {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $this->db->where($column, $value);
        $query = $this->db->get($this->name);
        return $query->row_array();
    }
    
    public function getOneByWhere($where){
        $this->db->where($where);
        $query = $this->db->get($this->name);
        return $query->row_array();
    }

    //Count all rows
    public function count() {
        return $this->db->count_all($this->name);
    }
    
    
    //Count with a certain condition
    public function countBy($column, $value) {
        $this->db->where($column, $value);
        $this->db->from($this->name);
        return $this->db->count_all_results();
    }
    
    //Count with a certain condition
    public function countByWhere($where) {
        $this->db->where($where);
        $this->db->from($this->name);
        return $this->db->count_all_results();
    }

    //Add a new row. Row is an array. returns the id of the inserted row
    public function add($row) {
        $this->db->insert($this->name, $row);
        return $this->db->insert_id();
    }

    //Updates a row given an id
    public function update($id, $row) {
        $this->db->where($this->key, $id);
        $this->db->update($this->name, $row);
    }
    
    //Updates a row with a certain condition
    public function updateWhere($where, $row) {
        $this->db->where($where);
        $this->db->update($this->name, $row);
    }

    //Delete a row with the id
    public function delete($id){
        return $this->db->delete($this->name, array('ID' => $id));
    }
    
    //Delete a row with a certain condition
    public function deleteBy($where) {
        return $this->db->delete($this->name, $where);
    }
    
    //Delete a row with a certain condition
    public function deleteAllBy($column,$where) {
        $this->db->where($column,$where);
        $query = $this->db->get($this->name);
        if($query->num_rows()>0){
            foreach ($query->result_array() as $rows) {
                $this->db->delete($this->name, array('ID' => $rows['ID']));
            }
        }
    }
    
    //Search rows from a column with a value
    public function search($column, $value){
        $this->db->like($column, $value);
        $this->db->order_by($this->orderby, "asc");
        $query = $this->db->get($this->name);
        return $query->result_array();
    }
    
    public function getAllBySingleJoin($join_table,$join_by,$where) {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $join_clause = $join_table.".".$join_by."=".$this->name.".".$join_by; //sample: category.cat_id = po_cat.cat_id

        $this->db->select('*');
        $this->db->from($this->name);
        $this->db->join($join_table,$join_clause);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }
    
    //Get all row given two column names and values. returns an array
    public function getAllByTwoColumns($column1, $value1,$column2,$value2) {
        $where = array($column1 => $value1, $column2 => $value2);
        return $this->getAllByWhere($where);
    }
    
    //Get a row given two column names and values. returns an array
    public function getOneByTwoColumns($column1, $value1,$column2,$value2) {
        if (isset($this->orderby) && $this->orderby != '') {
            $this->db->order_by($this->orderby);
        }
        $this->db->where($column1, $value1);
        $this->db->where($column2, $value2);
        $query = $this->db->get($this->name);
        return $query->row_array();
    }
    
    //for joining tables with different keys
    public function getAllBySingleJoinDiff($join_table,$join_by,$join_table_by,$where) {
//        if (isset($this->orderby) && $this->orderby != '') {
//            $this->db->order_by($this->orderby);
//        }
        $join_clause = $join_table.".".$join_by."=".$this->name.".".$join_table_by; 
        //sample: category.id = po_cat.cat_id
        $this->db->select('*');
        $this->db->from($this->name);
        $this->db->join($join_table,$join_clause);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result_array();
    }
    
}

?>
