<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Userpositions extends Table {

    function __construct() {
        parent::__construct();
    }

    public function get_position_by_id($userId) {
        $this->select();
        $this->from('tbluserposition');
        $this->where('user_id = ' . "'" . $userId . "'");
        $query = $this->get();
        return $query;
    }

    public function get_posdata($userId) {
        $query = $this->db->query('SELECT * FROM tbluserposition '
                . 'JOIN tblposition ON tbluserposition.position_id = tblposition.id '
                . 'WHERE tbluserposition.user_id=' . $userId);
        return $query->result_array();
    }

    public function save_userposition($data) {
        $result = $this->insert('tbluserposition', $data);
        return $result;
    }

    public function update_userpos($data, $userId) {
        $result = $this->update('tbluserposition', $data, 'user_id =' . $userId);
        return $result;
    }

}
