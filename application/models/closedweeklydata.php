<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Closedweeklydata extends Table {

    public function save_closed_data($data) {

        $result = $this->insert('tblclosedweeklydata', $data);
        return $result;
    }

    public function get_closed_data($user_id, $date, $location_id) {
        $query = $this->db->query('SELECT * FROM tblclosedweeklydata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_id . '
AND date = "' . $date . '" AND type=0');
        return $query->result_array();
    }

    public function get_closed_data_biller($user_id, $date, $location_id) {
        $query = $this->db->query('SELECT * FROM tblclosedweeklydata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_id . '
AND date = "' . $date . '" AND type=1');
        return $query->result_array();
    }

    public function get_all_closed() {
        $this->select('*');
        $this->from('tblclosedweeklydata');
        $query = $this->get();
        return $query;
    }

    public function get_all_closed_by($user_id, $location_id) {
        $query = $this->db->query('SELECT * FROM tblclosedweeklydata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_id .' AND type = 0');
        return $query->result_array();
    }
    
     public function get_all_closed_by_biller($user_id, $location_id) {
        $query = $this->db->query('SELECT * FROM tblclosedweeklydata
WHERE user_id = ' . $user_id . ' 
AND location_id = ' . $location_id .' AND type = 1');
        return $query->result_array();
    }

}
