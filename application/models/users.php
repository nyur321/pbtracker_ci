<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends Table {

    function __construct() {
        parent::__construct();
    }

    public function get_all() {
        $this->select();
        $this->from('tblusers');
        $query = $this->get();
        return $query;
    }
    
    public function  get_user_by_id($userId){
        $this->select();
        $this->from('tblusers');
        $this->where('id = '. $userId);
        $query = $this->get();
        return $query;
    }

    public function get_users_underlocs($userId) {
        $query = $this->db->query('SELECT tblusers.* FROM tbluserlocation 
            JOIN tblusers ON tbluserlocation.user_id = tblusers.id
            WHERE tbluserlocation.location_id = ANY(SELECT location_id
            FROM tbluserlocation WHERE tbluserlocation.user_id = ' . $userId . ') 
            GROUP BY tbluserlocation.user_id');
        return $query->result_array();
    }

    public function get_user_by_email($email) {
        $this->select();
        $this->from('tblusers');
        $this->where('email = ' . "'" . $email . "'");
        $query = $this->get();
        return $query;
    }

    public function save_userdata($data) {
        $result = $this->insert('tblusers', $data);
        return $result;
    }

    public function get_user_by_valcode($hash) {
        $this->select();
        $this->from('tblusers');
        $this->where('validation_code = ' . "'" . $hash . "'");
        $query = $this->get();
        return $query;
    }

    public function update_user($data, $userId) {
        $result = $this->update('tblusers', $data, 'id =' . $userId);
        return $result;
    }

    public function get_user_by_empid($empId) {
        $this->select();
        $this->from('tblusers');
        $this->where('employee_id = ' . "'" . $empId . "'");
        $query = $this->get();
        return $query;
    }

    public function get_users_under_prac($pid) {
        $this->select();
        $this->from('tbluserpractice as t1');
        $this->join('tblusers as t2', 't1.user_id = t2.id   ');
        $this->where('t1.practice_id = ' . "'" . $pid . "'" );
        $query = $this->get();
        return $query;
    }
    

}
