<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Collectionratehistory extends Table {
    public function get_collection_rate_history(){
        $this->select();
        $this->from('tblcollectionratehistory');
    }
    public function create_collection_rate_history($data){
        $this->insert('tblcollectionratehistory', $data);
    }
    public function get_collection_rate_history_by_location_id($locid){
        $query = $this->db->query('SELECT tblcollectionratehistory.collection_rate as collection_rate, tblcollectionratehistory.effective_date as effective_date, tblusers.first_name as first_name, tblusers.last_name as last_name
FROM tblcollectionratehistory
INNER JOIN tblcollectionrate ON tblcollectionratehistory.location_id = tblcollectionrate.location_id
INNER JOIN tblusers ON tblcollectionratehistory.updated_by = tblusers.id
WHERE tblcollectionratehistory.location_id ='.$locid);
        return $query->result_array();
    }
}