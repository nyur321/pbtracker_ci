<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Owner_registration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('collectionrates');
        $this->load->model('userlocations');
        $this->load->model('locations');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('mailer');
    }

    public function index() {
        if (!$_GET['hash']) {
            redirect('login');
        }
        
        $strHash = $_GET['hash'];
        $data = $this->get_page_data($strHash);

        if (!$data) {
            redirect('login');
        }

        $roles = $this->session->userdata('roles');

        $userData = array(
            'userId' => $data['userData'][0]['id'],
            'empId' => $data['userData'][0]['employee_id'],
            'firstName' => $data['userData'][0]['first_name'],
            'lastName' => $data['userData'][0]['last_name'],
            'email' => $data['userData'][0]['email'],
            'valCode' => $data['userData'][0]['validation_code']
        );

        $practiceData = array(
            'practiceID' => $data['practiceData'][0]['id'],
            'practiceName' => $data['practiceData'][0]['practice_name'],
            'practiceCode' => $data['practiceData'][0]['practice_code']
        );

        $this->load->vars($userData);
        $this->load->vars($practiceData);

        $this->template->set_layout('owner_registration');
        $this->template->title('Owner Registration');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->append_metadata('<script src="' . base_url("js/owner-registration.js") . '"></script>');
        $this->template->build('pages/owner_registration');
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function get_page_data($hash) {
        $userData = $this->users->get_user_by_valcode($hash);
        $practiceData = $this->practices->get_practice_by_userid($userData[0]['id']);

        $data = array(
            'userData' => $userData,
            'practiceData' => $practiceData
        );

        if ($userData) {
            return $data;
        } else {
            return false;
        }
    }

    //validations below
    public function page1_val() {
        $this->form_validation->set_message('is_unique', 'The %s has already been used.');
        $rules = array(
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'confirmpassword',
                'label' => 'Confirm Password',
                'rules' => 'trim|required|matches[password]'
            )
        );
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
                'password' => form_error('password'),
                'confirmpassword' => form_error('confirmpassword')
            );

            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'ok'
            );
            echo json_encode($data);
        }
    }

    public function page2_val() {
        $this->form_validation->set_message('is_unique', 'The %s has already been used.');
        $rules = array(
            array(
                'field' => 'emp-id',
                'label' => 'Employee ID',
                'rules' => 'trim|required|is_unique[tblusers.employee_id]'
            ),
            array(
                'field' => 'reg-fname',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'reg-lname',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'reg-email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|callback_check_email'
            ),
        );
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
                'empid' => form_error('emp-id'),
                'regfname' => form_error('reg-fname'),
                'reglname' => form_error('reg-lname'),
                'regemail' => form_error('reg-email')
            );

            echo json_encode($data);
        } else {
            $data = array(
                'status' => 'ok'
            );
            echo json_encode($data);
        }
    }

    public function page3_val() {
        $this->form_validation->set_message('is_unique', 'The %s has already been used.');
        $rules = array(
            array(
                'field' => 'practice-code',
                'label' => 'Practice Code',
                'rules' => 'trim|required|callback_check_pcode'
            ),
            array(
                'field' => 'practice-name',
                'label' => 'Practice Name',
                'rules' => 'trim|required|callback_check_pName'
            ),
            array(
                'field' => 'location-code',
                'label' => 'Location Code',
                'rules' => 'trim|required|is_unique[tbllocation.location_code]'
            ),
            array(
                'field' => 'location-name',
                'label' => 'Location Name',
                'rules' => 'trim|required|is_unique[tbllocation.location_name]'
            ),
            array(
                'field' => 'collection-rate',
                'label' => 'Collection Rate',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'start-date',
                'label' => 'Start date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'address1',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'city',
                'label' => 'City',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'state',
                'label' => 'State',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'zipcode',
                'label' => 'Zipcode',
                'rules' => 'trim|required'
            )
        );
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has already been used.');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
                'practicecode' => form_error('practice-code'),
                'practicename' => form_error('practice-name'),
                'locationcode' => form_error('location-code'),
                'locationname' => form_error('location-name'),
                'collectionrate' => form_error('collection-rate'),
                'startdate' => form_error('start-date'),
                'address1' => form_error('address1'),
                'city' => form_error('city'),
                'state' => form_error('state'),
                'zipcode' => form_error('zipcode')
            );

            echo json_encode($data);
        } else {//validation success
            //save data
            $this->save_owner_data();

            $data = array(
                'status' => 'ok'
            );
            echo json_encode($data);
        }
    }

    public function check_email() {
        $email = $this->input->post('reg-email');
        $userId = $this->input->post('user-id');

        $isUnique = $this->users->get_user_by_email($email);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $userId) {
                return true;
            } else {
                $this->form_validation->set_message('check_email', 'The %s is already registered to a different account.');
                return false;
            }
        }
    }

    public function check_pname() {
        $pName = $this->input->post('practice-name');
        $practiceId = $this->input->post('practice-id');

        $isUnique = $this->practices->get_practice_by_name($pName);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $practiceId) {
                return true;
            } else {
                $this->form_validation->set_message('check_pname', 'The %s has already been used.');
                return false;
            }
        }
    }

    public function check_pcode() {
        $pCode = $this->input->post('practice-code');
        $practiceId = $this->input->post('practice-id');

        $isUnique = $this->practices->get_practice_by_code($pCode);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $practiceId) {
                return true;
            } else {
                $this->form_validation->set_message('check_pcode', 'The %s has already been used.');
                return false;
            }
        }
    }

    public function save_owner_data() {
        $this->update_user();
        $this->update_practice();
        $this->save_location();
    }

    public function update_user() {

        $passMD5 = md5($this->input->post('password'));
        $passResetDate = date('Y-m-d', strtotime(date('Y-m-d').'+90 days'));

        $data = array(
//            'id' => $this->input->post('user-id'),
            'employee_id' => "'" . $this->input->post('emp-id') . "'",
            'first_name' => "'" . $this->input->post('reg-fname') . "'",
            'last_name' => "'" . $this->input->post('reg-lname') . "'",
            'email' => "'" . $this->input->post('reg-email') . "'",
//            'contact_no' => ,
            'password' => "'" . $passMD5 . "'",
            'password_reset_date' => "'" . $passResetDate . "'",
            'employment_status' => 1, //emp status 1 =active / 0 =inactive
            'activation_status' => 1, //activation status  1 = activated / 0 =not activated
//            'termination_date'=>'',
//            'timestamp'=>'',
        );


        $this->users->update_user($data, $this->input->post('user-id'));
    }

    public function update_practice() {

        $data = array(
//            'id' => $this->input->post('practice-id'),
//            'user_id' => $this->input->post('user-id'),
            'practice_code' => "'" . $this->input->post('practice-code') . "'",
            'practice_name' => "'" . $this->input->post('practice-name') . "'",
            'practice_status' => 1, //practice status 1 - active / 0 = inactive
//            'logo' = ,//not implemented yet
//            'timestamp'=>'',
        );
        $this->practices->update_practice($data, $this->input->post('practice-id'));
    }

    public function save_location() {

        $startdate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $data = array(
//           'id' => , 
            'practice_id' => $this->input->post('practice-id'),
            'location_code' => "'" . $this->input->post('location-code') . "'",
            'location_name' => "'" . $this->input->post('location-name') . "'",
            'address_1' => "'" . $this->input->post('address1') . "'",
            'address_2' => "'" . $this->input->post('address2') . "'",
            'city' => "'" . $this->input->post('city') . "'",
            'state' => "'" . $this->input->post('state') . "'",
            'zipcode' => $this->input->post('zipcode'),
            'is_main' => $this->input->post('office'),
            'start_date' => "'" . $startdate . "'",
//            'close_date' => ,
            'location_status' => 1, //location status 1 = active/ 0 = closed
            'reporting_mode' => $this->input->post('repmode'), /// 0 = daily / = weekly 
//            'timestamp' =>
        );
        //insert location
        $this->locations->save_location($data);
        $locId = mysql_insert_id();
        $this->save_userlocation($locId);
        $this->save_collectionrate($locId);
    }

    public function save_collectionrate($locId) {

        $startdate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $data = array(
//            'id'=>,
            'location_id' => $locId,
            'collection_rate' => $this->input->post('collection-rate'),
            'effective_date' => "'" . $startdate . "'",
        );
        $this->collectionrates->save_collectionrate($data);
    }

    public function save_userlocation($locId) {
        $startdate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $data = array(
//            'id'=>,
            'user_id' => $this->input->post('user-id'),
            'location_id' => $locId,
            'work_status' => 1, //1 = active, 0 = terminated
            'initial_cases' => 0,
            'start_date' => "'" . $startdate . "'"
//             'termination_date' =>
        );

        $this->userlocations->save_userlocation($data);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */