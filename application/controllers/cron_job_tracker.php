<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cron_job_tracker extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('users');
        $this->load->model('locations');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('positions');
        $this->load->model('mailer');
        $this->load->model('cliniciandata');
        $this->load->model('cliniciandatas');
        $this->load->model('collectionrates');
        $this->load->model('clinicianbaseline');
        $this->load->model('closedweeklydata');
    }

    public function index() {
        $start_date = date('Y-m-d');
        //for clinician roles


        $clinician_users = $this->userroles->get_all_role_clinicians();

        foreach ($clinician_users as $userdata) {
            $user_locations = $this->userlocations->get_locids($userdata['user_id']);
            $checked_user = $this->check_closed_weeks($userdata['user_id'], $userdata['location_id']);
            $userinfo = $this->users->get_user_by_id($userdata['user_id']);
            if ($checked_user['email'] == 'yes') {
                $users_sent[] = array(
                    'user_id' => $userdata['user_id'],
                    'empid' => $userinfo[0]['employee_id'],
                    'fname' => $userinfo[0]['first_name'],
                    'lname' => $userinfo[0]['last_name'],
                    'location_id' => $userdata['location_id'],
                    'type' => '0',
                    'dates' => $checked_user['last_week'],
                );
                // $this->send_mail($userdata['user_id'], $userdata['location_id'], $checked_user['last_week']);
            }
        }
        //for biller roles
        $biller_users = $this->userroles->get_all_role_billers();
        foreach ($biller_users as $userdata) {
            $user_locations = $this->userlocations->get_locids($userdata['user_id']);
            $checked_user = $this->check_closed_weeks($userdata['user_id'], $userdata['location_id']);
            $userinfo = $this->users->get_user_by_id($userdata['user_id']);
            if ($checked_user['email'] == 'yes') {
                $users_sent[] = array(
                    'user_id' => $userdata['user_id'],
                    'empid' => $userinfo[0]['employee_id'],
                    'fname' => $userinfo[0]['first_name'],
                    'lname' => $userinfo[0]['last_name'],
                    'location_id' => $userdata['location_id'],
                    'type' => '1',
                    'dates' => $checked_user['last_week'],
                );
                //  $this->send_mail_biller($userdata['user_id'], $userdata['location_id'], $checked_user['last_week']);
            }
        }


        $mail_data = $this->format_data_to_email($users_sent);
        $this->email_officers($mail_data);
    }

    public function check_closed_weeks($user_id, $location_id) {
        $closeweeklydat = $this->closedweeklydata->get_all_closed_by($user_id, $location_id);

        if (!empty($closeweeklydat)) {
            $previous_week = date('Y-m-d', strtotime("-1 week"));
            $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week +1 day"))));
            $last_week_dates_arr = $this->get_week_dates($previous_week);

//        echo $last_week_number;

            $return_array = array();
            $converted_week_num = array();


//        Destroys duplicate

            if (date('W') != '1') {
                $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week"))));
                foreach ($closeweeklydat as $key => $closeweek) {
                    $converted_week_num[$key] = date('W', strtotime($closeweek['date']));
                }

                $unique_week_nums = array_unique($converted_week_num);
//            print_r($unique_week_nums);
                foreach ($unique_week_nums as $closeweek) {
                    if ($closeweek == $last_week_number) {
//                    $return_array[$user_id][$location_id] = 'not_email';
                        $return_array = array(
                            'email' => 'no',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y') . 'W' . $last_week_number)))
                        );
                    } else {
                        $return_array = array(
                            'email' => 'yes',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y') . 'W' . $last_week_number)))
                        );
                    }
                }
//            echo $last_week_number;
            } else {
                $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week"))));
                foreach ($closeweeklydat as $key => $closeweek) {
                    $converted_week_num[$key] = date('W', strtotime($closeweek['date']));
                }
                $unique_week_nums = array_unique($converted_week_num);
                print_r($unique_week_nums);
                foreach ($unique_week_nums as $closeweek) {
                    if ($closeweek == $last_week_number) {
                        $return_array = array(
                            'email' => 'no',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y', strtotime('-1 year')) . 'W' . $last_week_number)))
                        );
                    } else {
                        $return_array = array(
                            'email' => 'yes',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y', strtotime('-1 year')) . 'W' . $last_week_number)))
                        );
                    }
                }

//            echo $last_week_number;
//            print_r($closeweek);
            }
        } else {
            $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week"))));

            if (date('W') != '1') {
                $return_array = array(
                    'email' => 'yes',
                    'user_id' => $user_id,
                    'location_id' => $location_id,
                    'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y') . 'W' . $last_week_number)))
                );
            } else {
                $return_array = array(
                    'email' => 'yes',
                    'user_id' => $user_id,
                    'location_id' => $location_id,
                    'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y', strtotime('-1 year')) . 'W' . $last_week_number)))
                );
            }
        }
        return $return_array;
    }

    public function check_closed_weeks_biller($user_id, $location_id) {
        $closeweeklydat = $this->closedweeklydata->get_all_closed_by_biller($user_id, $location_id);

        if (!empty($closeweeklydat)) {
            $previous_week = date('Y-m-d', strtotime("-1 week"));
            $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week +1 day"))));
            $last_week_dates_arr = $this->get_week_dates($previous_week);

//        echo $last_week_number;

            $return_array = array();
            $converted_week_num = array();


//        Destroys duplicate

            if (date('W') != '1') {
                $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week"))));
                foreach ($closeweeklydat as $key => $closeweek) {
                    $converted_week_num[$key] = date('W', strtotime($closeweek['date']));
                }

                $unique_week_nums = array_unique($converted_week_num);
//            print_r($unique_week_nums);
                foreach ($unique_week_nums as $closeweek) {
                    if ($closeweek == $last_week_number) {
//                    $return_array[$user_id][$location_id] = 'not_email';
                        $return_array = array(
                            'email' => 'no',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y') . 'W' . $last_week_number)))
                        );
                    } else {
                        $return_array = array(
                            'email' => 'yes',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y') . 'W' . $last_week_number)))
                        );
                    }
                }
//            echo $last_week_number;
            } else {
                $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week"))));
                foreach ($closeweeklydat as $key => $closeweek) {
                    $converted_week_num[$key] = date('W', strtotime($closeweek['date']));
                }
                $unique_week_nums = array_unique($converted_week_num);
                print_r($unique_week_nums);
                foreach ($unique_week_nums as $closeweek) {
                    if ($closeweek == $last_week_number) {
                        $return_array = array(
                            'email' => 'no',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y', strtotime('-1 year')) . 'W' . $last_week_number)))
                        );
                    } else {
                        $return_array = array(
                            'email' => 'yes',
                            'user_id' => $user_id,
                            'location_id' => $location_id,
                            'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y', strtotime('-1 year')) . 'W' . $last_week_number)))
                        );
                    }
                }

//            echo $last_week_number;
//            print_r($closeweek);
            }
        } else {
            $last_week_number = date('W', strtotime(date('Y-m-d', strtotime("-1 week"))));

            if (date('W') != '1') {
                $return_array = array(
                    'email' => 'yes',
                    'user_id' => $user_id,
                    'location_id' => $location_id,
                    'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y') . 'W' . $last_week_number)))
                );
            } else {
                $return_array = array(
                    'email' => 'yes',
                    'user_id' => $user_id,
                    'location_id' => $location_id,
                    'last_week' => $this->range_week(date('Y-m-d', strtotime(date('Y', strtotime('-1 year')) . 'W' . $last_week_number)))
                );
            }
        }
//        print_r($return_array);
        return $return_array;
    }

    public function range_week($datestr) {
        $dt = strtotime($datestr);
        $res['start'] = date('N', $dt) == 1 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('last monday', $dt));
        $res['end'] = date('N', $dt) == 7 ? date('Y-m-d', $dt) : date('Y-m-d', strtotime('next sunday', $dt));
        return $res;
    }

    public function get_week_dates($date) {
        list($day, $month, $year) = explode("-", $date);

        // Get the weekday of the given date
        $wkday = date('l', mktime('0', '0', '0', $month, $day, $year));

        switch ($wkday) {
            case 'Monday': $numDaysToMon = 0;
                break;
            case 'Tuesday': $numDaysToMon = 1;
                break;
            case 'Wednesday': $numDaysToMon = 2;
                break;
            case 'Thursday': $numDaysToMon = 3;
                break;
            case 'Friday': $numDaysToMon = 4;
                break;
            case 'Saturday': $numDaysToMon = 5;
                break;
            case 'Sunday': $numDaysToMon = 6;
                break;
        }

        // Timestamp of the monday for that week
        $monday = mktime('0', '0', '0', $month, $day - $numDaysToMon, $year);

        $seconds_in_a_day = 86400;

        // Get date for 7 days from Monday (inclusive)
        for ($i = 0; $i < 7; $i++) {
            $dates[$i] = date('m/d/Y', $monday + ($seconds_in_a_day * $i));
        }

        for ($x = 1; $x < 8; $x++) {
            $final_dates[$x] = $dates[$x - 1];
        }
//        print_r($);
        return $final_dates;
    }

    public function send_mail($user_id, $location_id, $range_date) {
        $user_data = $this->users->get_user_by_id($user_id);
        $location_data = $this->locations->get_location_by_id($location_id);

        $data = array(
            'pbLogoLink' => base_url() . 'img/pb-logo-2.jpg',
            'headerBgLink' => base_url() . 'img/nav-bg.png',
            'firstName' => $user_data[0]['first_name'],
            'lastName' => $user_data[0]['last_name'],
            'location_name' => $location_data[0]['location_name'],
            'siteurl' => site_url(),
            'start_date' => $range_date['start'],
            'end_date' => $range_date['end'],
            'dashboard' => 'Clinician\'s Dashboard',
            'table_name' => 'Volume ',
        );

        $mailBody = $this->load->view('templates/cron_job_send_email', $data, true);
        return $this->mailer->send_mail(//send mail function in mailer.php
                        $user_data[0]['email'], //destination email
                        'Performance Tracker - Close Weekly Data', //subject
                        $mailBody//email body
        );
    }

    function format_data_to_email($users_sent) {
        $locations = array();
        foreach ($users_sent as $user) {
            if (!in_array($user['location_id'], $locations)) {
                $locations[] = $user['location_id'];
            }
        }

        $separated_by_location = array(); // separated by location
        foreach ($locations as $location) {
            foreach ($users_sent as $user) {
                if ($user['location_id'] == $location) {
                    $separated_by_location[$location][$user['user_id']] = array(
                        'user_id' => $user['user_id'],
                        'empid' => $user['empid'],
                        'fname' => $user['fname'],
                        'lname' => $user['lname'],
                        'type' => $user['type'],
                        'dates' => $user['dates'],
                    );
                }
            }
        }

        $practices = array();
        foreach ($separated_by_location as $location_id => $location_data) {
            $practiceid = $this->get_practice_by_locationid($location_id);
            if (!in_array($practiceid[0]['practice_id'], $practices)) {
                $practices[] = $practiceid[0]['practice_id'];
            }
        }

        $separated_by_practice = array();
        foreach ($practices as $practice) {
            foreach ($separated_by_location as $location_id => $location_data) {
                $practiceid = $this->get_practice_by_locationid($location_id);
                if ($practiceid[0]['practice_id'] == $practice) {

                    $separated_by_practice[$practice][$location_id] = array(
                        'location_name' => $practiceid[0]['location_name'],
                        'location_data' => $location_data
                    );
                }
            }
        }
        return $separated_by_practice;
    }

    function email_officers($mail_data) {
        $employees = array();


        foreach ($mail_data as $practice_id => $locations) {
            $practicename = $this->get_practice_name_by_pid($practice_id);
            echo $practice_id . ' - ' . $practicename[0]['practice_name'];
            echo '<br/>';
            foreach ($locations as $location_id => $location_data) {
                echo $location_id . ' - ' . $location_data['location_name'];
                echo '<br/>';
                foreach ($location_data['location_data'] as $user) {
                    if ($user['type'] == '0') {
                        $employees[] = array(
                            'id' => $user['empid'],
                            'name' => $user['fname'] . ' ' . $user['lname'],
                            'location' => $location_data['location_name'],
                            'role' => 'Clinician',
                        );
                    } else {
                        $employees[] = array(
                            'id' => $user['empid'],
                            'name' => $user['fname'] . ' ' . $user['lname'],
                            'location' => $location_data['location_name'],
                            'role' => 'Biller',
                        );
                    }
                }
            }
            echo '<br/>';
        }

        print_r(json_encode($employees));
    }

    function get_practice_name_by_pid($pid) {
        return $this->practices->get_practice_by_id($pid);
    }

    function email_admin() {
        
    }

    function email_owner() {
        
    }

    function get_practice_by_locationid($location_id) {
        return $this->locations->get_practice_by_locid($location_id);
    }

    function send_mail_biller($user_id, $location_id, $range_date) {
        $user_data = $this->users->get_user_by_id($user_id);
        $location_data = $this->locations->get_location_by_id($location_id);

        $data = array(
            'pbLogoLink' => base_url() . 'img/pb-logo-2.jpg',
            'headerBgLink' => base_url() . 'img/nav-bg.png',
            'firstName' => $user_data[0]['first_name'],
            'lastName' => $user_data[0]['last_name'],
            'location_name' => $location_data[0]['location_name'],
            'siteurl' => site_url(),
            'start_date' => $range_date['start'],
            'end_date' => $range_date['end'],
            'dashboard' => 'Biller\'s Dashboard',
            'table_name' => '',
        );

        $mailBody = $this->load->view('templates/cron_job_send_email', $data, true);
        return $this->mailer->send_mail(//send mail function in mailer.php
                        $user_data[0]['email'], //destination email
                        'Performance Tracker - Close Weekly Data', //subject
                        $mailBody//email body
        );
    }

}
