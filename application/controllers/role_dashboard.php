<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Role_dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('cliniciandatas');
        $this->load->model('userlocations');
        $this->load->model('users');
        $this->load->model('collectionrates');
    }

    public function index() {

        $this->template->set_layout('default');
        $this->template->title('Performance Tracker | Role Dashboard');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
//        $this->template->append_metadata('<script src="' . base_url("js/admin-dashboard.js") . '"></script>');
        $this->template->build('pages/role_dashboard');
    }
}