<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Migrate extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->db_live = $this->load->database('live', TRUE);
        $this->db_gamma = $this->load->database('gamma', TRUE);
        $this->db = $this->load->database('default', TRUE);
        $this->load->model('users');
    }

//    public function migrate_data() {
//        //tblusers
////        $this->db_live->db_select();
////        $sql = 'SELECT * FROM tblusers;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $user) {
////            $data = array(
////                'id' => $user['id'],
////                'employee_id' => "'" . $user['employee_id'] . "'",
////                'first_name' => "'" . $user['first_name'] . "'",
////                'last_name' => "'" . $user['last_name'] . "'",
////                'email' => "'" . $user['email'] . "'",
////                'contact_no' => $user['contact_no'],
////                'password' => "'" . $user['password'] . "'",
////                'password_reset_date' => "'" . $user['password_reset_date'] . "'",
////                'employment_status' => $user['employee_status'],
////                'validation_code' => "'" . $user['validation_code'] . "'",
////                'activation_status' => $user['activated'],
////                'termination_date' => "'" . $user['lastday'] . "'",
////                'timestamp' => "'" . $user['timestamp'] . "'",
////            );
////
////            $this->insert('tblusers', $data);
////        }
//        //tbluserrole
////        $this->db_live->db_select();
////        $sql = 'SELECT * FROM tbluserrole;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $userrole) {
////            if($userrole['user_id'] != 11){
////             $data = array(
////                 'user_id'=>$userrole['user_id'],
////                 'role_id'=>$userrole['role_id']
////             );
////             $this->insert('tbluserrole', $data);
////            }
////        }
//        //tbluserpractice
////        $this->db_live->db_select();
////        $sql = 'SELECT * FROM tbluserpractice;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $userprac) {
////            if($userprac['user_id'] != 11){
////             $data = array(
////                 'user_id'=>$userprac['user_id'],
////                 'practice_id'=>$userprac['practice_id']
////             );
////             $this->insert('tbluserpractice', $data);
////            }
////        }
//        //tbluserposition
////                $this->db_live->db_select();
////        $sql = 'SELECT * FROM tbluserposition;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $userpos) {
////            if($userpos['user_id'] != 11){
////             $data = array(
////                 'user_id'=>$userpos['user_id'],
////                 'position_id'=>$userpos['position_id']
////             );
////             $this->insert('tbluserposition', $data);
////            }
////        }
////        
//        //tbluserlocation
////        $this->db_live->db_select();
////        $sql = 'SELECT * FROM tbluserlocation;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $userloc) {
////            if ($userloc['user_id'] != 11) {
////                $this->db_live->db_select();
////                $sql2 = 'SELECT * FROM tblclinicianbaseline WHERE user_id=' . $userloc['user_id'] . ' AND location_id=' . $userloc['location_id'];
////                $result2 = $this->db_live->query($sql2)->result_array();
////                
////                $data = array(
////                    'user_id' => $userloc['user_id'],
////                    'location_id' => $userloc['location_id'],
////                    'work_status' => $result2[0]['work_status'],
////                    'initial_cases' => $result2[0]['baseline'],
////                    'start_date' => "'" . date('Y-m-d',strtotime($result2[0]['baseline_date'] . "+1 days")) . "'",
////                    'termination_date' => "'" . $result2[0]['termination_date'] . "'",
////                );
////                
////                $this->insert('tbluserlocation', $data);
////            }
////        }
//        //tblpractice
////                $this->db_live->db_select();
////        $sql = 'SELECT * FROM tblpractice;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $prac) {
////            if($prac['user_id'] != 11 || $prac['user_id']!=12){
////             $data = array(
////                 'user_id'=>$prac['user_id'],
////                 'practice_code'=>"'".$prac['practice_code']."'",
////                 'practice_name'=>"'".$prac['practice_name']."'",
////                 'practice_status'=>$prac['status'],
////                 'timestamp'=>"'".$prac['timestamp']."'",
////             );
////             $this->insert('tblpractice', $data);
////            }
////        }
////        
//        //tbllocation
////        $this->db_live->db_select();
////        $sql = 'SELECT * FROM tbllocation;';
////        $result = $this->db_live->query($sql)->result_array();
////        foreach ($result as $loc) {
////
////            $data = array(
////                'id'=>$loc['id'],
////                'practice_id' => $loc['practice_id'],
////                'location_code' => "'" . $loc['location_code'] . "'",
////                'location_name ' => "'" . $loc['location_name'] . "'",
////                'address_1' => "'".$loc['address1']."'",
////                'address_2' => "'" . $loc['address2'] . "'",
////                'city' => "'".$loc['city']."'",
////                'state' => "'" . $loc['state'] . "'",
////                'zipcode' => $loc['zipcode'],
////                'is_main' => $loc['is_mainoffice'],
////                'start_date' => "'".$loc['baseline_date']."'",
////                'close_date' => "'" . $loc['closing_date']. "'",
////                'location_status' => $loc['status'],
////                'reporting_mode' => 0,
////            );
////            
////            $this->insert('tbllocation', $data);
////              
////              
////            $data2 = array(
////                'location_id'=>$loc['id'],
////                'collection_rate'=>$loc['collection_rate'],
////                'effective_date'=>"'".date('Y-m-d', strtotime($loc['timestamp'])) ."'",
////            );
////          
////            $this->insert('tblcollectionrate', $data2);
////        }
//        //tblcliniciandata
////        $this->db_live->db_select();
////        $sql = 'SELECT * FROM tblcliniciandata;';
////        $result = $this->db_live->query($sql)->result_array();
//////        $index = 7;
////        foreach ($result as $clinic) {
////            
////            $data = array(
//////                'id'=>$index,
////                'user_id' => $clinic['user_id'],
////                'location_id' => $clinic['location_id'],
////                'start_date' => "'" . $clinic['startdate'] . "'",
////                'end_date' => "'" . $clinic['enddate'] . "'",
////                'new_cases' => $clinic['new_cases'],
////                'active_discharges' => $clinic['active_discharges'],
////                'passive_discharges' => $clinic['passive_discharges'],
//////                  'added_cases'=>$clinic['location_id'],
//////                  'removed_cases'=>$clinic['location_id'],
////                'active_cases' => $clinic['active_cases'],
////                'visits_attended' => $clinic['visits_attended'],
////                'no_show' => $clinic['no_show'],
////                'visits_scheduled' => $clinic['no_show'] + $clinic['visits_attended'],
////                'units_charged' =>  $clinic['units_charged'],
////                'days_worked' => $clinic['days_worked'],
////                'hours_worked' =>  $clinic['hours_worked'] ,
////                'hours_scheduled' => $clinic['hours_scheduled'] ,
////                'charges' => $clinic['charges'],
////                'billing_days' => $clinic['billing_days'],
////            );
//////                $index++;
////            $this->insert('tblcliniciandata', $data);
////        }
//    }

    public function migrate_data() {
        //tblusers
//        $this->db_gamma->db_select();
//        $sql = 'SELECT t1.*, t2.location_id FROM tblusers as t1 join tbluserlocation as t2 on t1.id=t2.user_id Group BY t1.id;';
//        $result = $this->db_gamma->query($sql)->result_array();
//
//        foreach ($result as $user) {
//                if ($user['location_id'] == 20 || $user['location_id'] == 21 || $user['location_id'] == 22 ||$user['location_id'] == 29 || $user['location_id'] == 43) {
//                    $data = array(
//                        'id' => $user['id'] + 30,
//                        'employee_id' => "'" . $user['employee_id'] . "'",
//                        'first_name' => "'" . $user['first_name'] . "'",
//                        'last_name' => "'" . $user['last_name'] . "'",
//                        'email' => "'" . $user['email'] . "'",
//                        'contact_no' => $user['contact_no'],
//                        'password' => "'" . $user['password'] . "'",
//                        'password_reset_date' => "'" . $user['password_reset_date'] . "'",
//                        'employment_status' => $user['employee_status'],
//                        'validation_code' => "'" . $user['validation_code'] . "'",
//                        'activation_status' => $user['activated'],
//                        'termination_date' => "'" . $user['lastday'] . "'",
//                        'timestamp' => "'" . $user['timestamp'] . "'",
//                    );
//
//                    $this->insert('tblusers', $data);
//                }
//            
//        }
        //tbluserrole
//        $this->db_gamma->db_select();
//         $sql = 'SELECT t1.*, t2.location_id FROM tbluserrole as t1 join tbluserlocation as t2 on t1.user_id=t2.user_id Group BY t1.id;';
//        $result = $this->db_gamma->query($sql)->result_array();
//        foreach ($result as $userrole) {
//           if ($userrole['location_id'] == 20 || $userrole['location_id'] == 21 || $userrole['location_id'] == 22 ||$userrole['location_id'] == 29 || $userrole['location_id'] == 43) {
//             $data = array(
//                 'user_id'=>$userrole['user_id']+30,
//                 'role_id'=>$userrole['role_id']
//             );
//             $this->insert('tbluserrole', $data);
//            }
//        }
        //tbluserpractice
//        $this->db_gamma->db_select();
//        $sql = 'SELECT * FROM tbluserpractice;';
//        $result = $this->db_gamma->query($sql)->result_array();
//        foreach ($result as $userprac) {
//            if($userprac['practice_id']==27 || $userprac['practice_id']==32 ||$userprac['practice_id']==60 ){
//             $data = array(
//                 'user_id'=>$userprac['user_id']+30,
//                 'practice_id'=>$userprac['practice_id']
//             );
//             $this->insert('tbluserpractice', $data);
//        }
//        }
        //tbluserposition
//        $this->db_gamma->db_select();
//  $sql = 'SELECT t1.*, t2.location_id FROM tbluserposition as t1 join tbluserlocation as t2 on t1.user_id=t2.user_id Group BY t1.id;';
//        $result = $this->db_gamma->query($sql)->result_array();
//        foreach ($result as $userpos) {
//       if ($userpos['location_id'] == 20 || $userpos['location_id'] == 21 || $userpos['location_id'] == 22 ||$userpos['location_id'] == 29 || $userpos['location_id'] == 43) {
//             $data = array(
//                 'user_id'=>$userpos['user_id'] +30,
//                 'position_id'=>$userpos['position_id']
//             );
//             $this->insert('tbluserposition', $data);
//       }
//        }
        //tbluserlocation
//        $this->db_gamma->db_select();
//        $sql = 'SELECT * FROM tbluserlocation;';
//        $result = $this->db_gamma->query($sql)->result_array();
//        foreach ($result as $userloc) {
//           if ($userloc['location_id'] == 20 || $userloc['location_id'] == 21 || $userloc['location_id'] == 22 ||$userloc['location_id'] == 29 || $userloc['location_id'] == 43) {
//                $this->db_gamma->db_select();
//                $sql2 = 'SELECT * FROM tblclinicianbaseline WHERE user_id=' . $userloc['user_id'] . ' AND location_id=' . $userloc['location_id'];
//                $result2 = $this->db_gamma->query($sql2)->result_array();
//                
//                $data = array(
//                    'user_id' => $userloc['user_id']+30,
//                    'location_id' => $userloc['location_id'],
//                    'work_status' => $result2[0]['work_status'],
//                    'initial_cases' => $result2[0]['baseline'],
//                    'start_date' => "'" . date('Y-m-d',strtotime($result2[0]['baseline_date'] . "+1 days")) . "'",
//                    'termination_date' => "'" . $result2[0]['termination_date'] . "'",
//                );
//                
//                $this->insert('tbluserlocation', $data);
//            }
//        }
        //tblpractice
//        
//         $this->db_gamma->db_select();
//        $sql = 'SELECT * FROM tblpractice;';
//        $result = $this->db_gamma->query($sql)->result_array();
//        foreach ($result as $prac) {
//            if($prac['id'] == 27 || $prac['id']==32 || $prac['id']==60){
//             $data = array(
//                 'id'=>$prac['id'],
//                 'user_id'=>$prac['user_id'] + 30,
//                 'practice_code'=>"'".$prac['practice_code']."'",
//                 'practice_name'=>"'".$prac['practice_name']."'",
//                 'practice_status'=>$prac['status'],
//                 'timestamp'=>"'".$prac['timestamp']."'",
//             );
//             $this->insert('tblpractice', $data);
//            }
//        }
//        
        //tbllocation
//        $this->db_gamma->db_select();
//        $sql = 'SELECT * FROM tbllocation;';
//        $result = $this->db_gamma->query($sql)->result_array();
//        foreach ($result as $loc) {
//            if($loc['practice_id']== 27|| $loc['practice_id'] == 32 || $loc['practice_id']==60){
//            $data = array(
//                'id'=>$loc['id'],
//                'practice_id' => $loc['practice_id'],
//                'location_code' => "'" . $loc['location_code'] . "'",
//                'location_name ' => "'" . $loc['location_name'] . "'",
//                'address_1' => "'".$loc['address1']."'",
//                'address_2' => "'" . $loc['address2'] . "'",
//                'city' => "'".$loc['city']."'",
//                'state' => "'" . $loc['state'] . "'",
//                'zipcode' => $loc['zipcode'],
//                'is_main' => $loc['is_mainoffice'],
//                'start_date' => "'".$loc['baseline_date']."'",
//                'close_date' => "'" . $loc['closing_date']. "'",
//                'location_status' => $loc['status'],
//                'reporting_mode' => 0,
//            );
//            
//            $this->insert('tbllocation', $data);
//            
//              
//            $data2 = array(
//                'location_id'=>$loc['id'],
//                'collection_rate'=>$loc['collection_rate'],
//                'effective_date'=>"'".date('Y-m-d', strtotime($loc['timestamp'])) ."'",
//            );
//          
//            $this->insert('tblcollectionrate', $data2);
//            
//            }
//        }
        //tblcliniciandata
//        $this->db_gamma->db_select();
//        $sql = 'SELECT * FROM tblcliniciandata;';
//        $result = $this->db_gamma->query($sql)->result_array();
////        $index = 7;
//        foreach ($result as $clinic) {
//            if ($clinic['location_id'] == 20 || $clinic['location_id'] == 21 || $clinic['location_id'] == 22 || $clinic['location_id'] == 29 || $clinic['location_id'] == 43) {
//                $data = array(
////                'id'=>$index,
//                    'user_id' => $clinic['user_id'] + 30,
//                    'location_id' => $clinic['location_id'],
//                    'start_date' => "'" . $clinic['startdate'] . "'",
//                    'end_date' => "'" . $clinic['enddate'] . "'",
//                    'new_cases' => $clinic['new_cases'],
//                    'active_discharges' => $clinic['active_discharges'],
//                    'passive_discharges' => $clinic['passive_discharges'],
////              'added_cases'=>$clinic['location_id'],
////              'removed_cases'=>$clinic['location_id'],
//                    'active_cases' => $clinic['active_cases'],
//                    'visits_attended' => $clinic['visits_attended'],
//                    'no_show' => $clinic['no_show'],
//                    'visits_scheduled' => $clinic['no_show'] + $clinic['visits_attended'],
//                    'units_charged' => $clinic['units_charged'],
//                    'days_worked' => 1,
//                    'hours_worked' => $clinic['hours_worked'],
//                    'hours_scheduled' => $clinic['hours_scheduled'],
//                    'charges' =>  $clinic['charges'],
//                    'billing_days' => 1,
//                );
////                $index++;
//                $this->insert('tblcliniciandata', $data);
//            }
//        }
    }

    public function insert($table, $data) {
        $this->db->db_select();

        $sql = "INSERT INTO " . $table;
        if (is_array($data)) {
            $fields = array_keys($data);
            $values = array_values($data);
            $sql .= " (" . implode(",", $fields) . ")";
            $sql .= " VALUES (" . implode(",", $values) . ")";
        } else {
            $sql .= " " . $data;
        }
        $result = $this->db->query($sql);

        return $result;
    }

}
