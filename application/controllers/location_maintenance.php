<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location_maintenance extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('positions');
        $this->load->model('mailer');
        $this->load->model('locations');
        $this->load->model('collectionrates');
        $this->load->model('collectionratehistory');
    }

    public function check_if_authorized() {
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $this->check_if_authorized();

        $practice_id = $this->session->userdata('practice_id');
        $role = $this->session->userdata('roles');
        if ($role[0]['role_id'] == '1') {
            $locations = $this->locations->get_all_locations();
            $locations_closed = $this->collectionrates->get_all_locations_in_closed();
        } else if ($role[0]['role_id'] == '2') {
            $locations = $this->locations->get_locations_in_practice($practice_id);
            $locations_closed = $this->collectionrates->get_locations_in_closed($practice_id);
        } else if ($role[0]['role_id'] == '3') {
            $user_id = $this->session->userdata('user_id');
            $locations = $this->locations->get_locations_by_user($user_id);
            $locations_closed = $this->collectionrates->get_locations_in_closed_by_user($user_id);
        }

        $data = array(
            'locations' => $locations,
            'locations_closed' => $locations_closed
        );

        $this->load->vars($data);
        $this->template->set_layout('default');
        $this->template->title('Location Maintenance');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/location-maintenance.js") . '"></script>');
        $this->template->build('pages/location_maintenance');
    }

    public function add_location() {
        $this->template->set_layout('default');
        $this->template->title('Performance Tracker | Add Location');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/add_location.js") . '"></script>');
        $this->template->build('pages/add_location');
    }

    public function add_location_submit() {
        $startDate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $location_data = array(
            'practice_id' => $this->session->userdata('practice_id'),
            'location_code' => "'" . $this->input->post('location-code') . "'",
            'location_name' => "'" . $this->input->post('location-name') . "'",
            'address_1' => "'" . $this->input->post('address1') . "'",
            'address_2' => "'" . $this->input->post('address2') . "'",
            'city' => "'" . $this->input->post('city') . "'",
            'state' => "'" . $this->input->post('state') . "'",
            'zipcode' => $this->input->post('zip-code'),
            'is_main' => $this->input->post('main-office'),
            'start_date' => "'" . $startDate . "'",
            'close_date' => "'" . '0000-00-00' . "'",
            'location_status' => '1',
            'reporting_mode' => $this->input->post('reporting-mode')
        );
        $this->locations->save_location($location_data);
        $locId = mysql_insert_id();
        $this->save_collectionrate($locId);
    }

    public function edit_location() {
        $location_id = $this->input->post('locid');
        $edit_locations = $this->locations->get_location_collection_history_by_id($location_id);
        $collection_rate_history = $this->collectionratehistory->get_collection_rate_history_by_location_id($location_id);
        $coll_rate_history = array(
            'coll_his' => $collection_rate_history
        );
        $coll_his = $this->set_collection_rate($coll_rate_history);

        $data = array(
            'edit_locations' => $edit_locations,
            'collection_rate_history' => $coll_his
        );

        echo json_encode($data);
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

    public function set_collection_rate($data) {
        $str_html = $this->load->view('templates/collection_history', $data, true);
        return $str_html;
    }

    public function get_collection_history($loc_id) {
        return $this->locations->get_collection_history($loc_id);
        ;
    }

    public function submit_edited_location() {
        if ($this->input->post('status') == '1') {
            $rules = array(
                array(
                    'field' => 'location-code',
                    'label' => 'Location Code',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'location-name',
                    'label' => 'Location Name',
                    'rules' => 'trim|required|callback_check_lname'
                ),
                array(
                    'field' => 'address1',
                    'label' => 'Address 1',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'city',
                    'label' => 'City',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'state',
                    'label' => 'State',
                    'rules' => 'trim|required|callback_state_check'
                ),
                array(
                    'field' => 'zip-code',
                    'label' => 'Zip Code',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'collection-rate',
                    'label' => 'Collection Rate',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'date-effective',
                    'label' => 'Date Effective',
                    'rules' => 'trim|required'
                )
            );
        } else {
            $rules = array(
                array(
                    'field' => 'location-code',
                    'label' => 'Location Code',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'location-name',
                    'label' => 'Location Name',
                    'rules' => 'trim|required|callback_check_lname'
                ),
                array(
                    'field' => 'address1',
                    'label' => 'Address 1',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'city',
                    'label' => 'City',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'state',
                    'label' => 'State',
                    'rules' => 'trim|required|callback_state_check'
                ),
                array(
                    'field' => 'zip-code',
                    'label' => 'Zip Code',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'close-date',
                    'label' => 'Close Date',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'collection-rate',
                    'label' => 'Collection Rate',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'date-effective',
                    'label' => 'Date Effective',
                    'rules' => 'trim|required'
                )
            );
        }
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has been already used');
        if ($this->form_validation->run() == false) {
            if ($this->input->post('status') == '1') {
                $data = array(
                    'status' => 'error1',
                    'locationcode' => form_error('location-code'),
                    'locationname' => form_error('location-name'),
                    'address1' => form_error('address1'),
                    'city' => form_error('city'),
                    'state' => form_error('state'),
                    'zipcode' => form_error('zip-code'),
                    'collectionrate' => form_error('collection-rate'),
                    'dateeffective' => form_error('date-effective')
                );
            } else {
                $data = array(
                    'status' => 'error1',
                    'locationcode' => form_error('location-code'),
                    'locationname' => form_error('location-name'),
                    'address1' => form_error('address1'),
                    'city' => form_error('city'),
                    'state' => form_error('state'),
                    'zipcode' => form_error('zip-code'),
                    'closedate' => form_error('close-date'),
                    'collectionrate' => form_error('collection-rate'),
                    'dateeffective' => form_error('date-effective')
                );
            }
            echo json_encode($data);
        } else {
            $this->save_edited_location();
            $data = array(
                'status' => 'success',
                'nyur' => $this->input->post('locid')
            );
            echo json_encode($data);
        }
    }

    public function save_edited_location() {
        if ($this->input->post('close-date')) {
            $close_date = date('Y-m-d', strtotime($this->input->post('close-date')));
        } else {
            $close_date = '0000-00-00';
        }


        $effective_date = date('Y-m-d', strtotime($this->input->post('date-effective')));
        $locid = $this->input->post('locid');
        $vals = array(
            'location_code' => "'" . $this->input->post('location-code') . "'",
            'location_name' => "'" . $this->input->post('location-name') . "'",
            'address_1' => "'" . $this->input->post('address1') . "'",
            'address_2' => "'" . $this->input->post('address2') . "'",
            'city' => "'" . $this->input->post('city') . "'",
            'state' => "'" . $this->input->post('state') . "'",
            'zipcode' => $this->input->post('zip-code'),
            'is_main' => $this->input->post('main-office'),
            'location_status' => $this->input->post('status'),
            'close_date' => "'" . $close_date . "'",
            'reporting_mode' => $this->input->post('reporting-mode')
        );
        $col_history_result = $this->collectionrates->get_current_collection_rate($locid);

        $collection_rate_history_vars = array(
            'collection_rate' => $col_history_result[0]['collection_rate'],
            'location_id' => $locid,
            'updated_by' => $this->session->userdata('user_id'),
            'effective_date' => "'" . $col_history_result[0]['effective_date'] . "'"
        );
        $collection_rate_vars = array(
            'collection_rate' => $this->input->post('collection-rate'),
            'effective_date' => "'" . $effective_date . "'",
            'location_id' => $locid
        );
        if ($col_history_result[0]['collection_rate'] != $this->input->post('collection-rate')) {
            $this->collectionratehistory->create_collection_rate_history($collection_rate_history_vars);
        }
        $result = $this->locations->update_location($vals, $locid);
        $result2 = $this->collectionrates->update_collection($collection_rate_vars, $locid);
        return $result;
    }

    public function save_collectionrate($locId) {

        $startdate = date('Y-m-d', strtotime($this->input->post('start-date')));
        $data = array(
//            'id'=>,
            'location_id' => $locId,
            'collection_rate' => $this->input->post('collection-rate'),
            'effective_date' => "'" . $startdate . "'",
        );
        $this->collectionrates->save_collectionrate($data);
    }

    public function submit_location() {

        $rules = array(
    
            array(
                'field' => 'location-code',
                'label' => 'Location Code',
                'rules' => 'trim|required|is_unique[tbllocation.location_code]'
            ),
            array(
                'field' => 'location-name',
                'label' => 'Location Name',
                'rules' => 'trim|required|is_unique[tbllocation.location_name]'
            ),
            array(
                'field' => 'start-date',
                'label' => 'start date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'collection-rate',
                'label' => 'Collection Rate',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'address1',
                'label' => 'Address 1',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'city',
                'label' => 'City',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'state',
                'label' => 'State',
                'rules' => 'trim|required|callback_state_check'
            ),
            array(
                'field' => 'zip-code',
                'label' => 'Zip Code',
                'rules' => 'trim|required'
            )
        );

        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has been already used');
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'error1',
 
                'locationcode' => form_error('location-code'),
                'locationname' => form_error('location-name'),
                'startdate' => form_error('start-date'),
                'collectionrate' => form_error('collection-rate'),
                'address1' => form_error('address1'),
                'city' => form_error('city'),
                'state' => form_error('state'),
                'zipcode' => form_error('zip-code'),
            );


            echo json_encode($data);
        } else {
            $this->add_location_submit();

            $data = array(
                'status' => 'success'
            );
            echo json_encode($data);
        }
    }

    public function state_check($str) {
        if ($str == '0') {
            $this->form_validation->set_message('state_check', 'You must select a state');
            return false;
        } else {
            return true;
        }
    }

    public function location_code_check($str) {

        $this->form_validation->set_message('location_code_check', 'This %s has been already used');

        return false;
    }

    public function check_lname($location_name) {
        $loc_id = $this->input->post('locid');
        $this->form_validation->set_message('check_lname', 'The %s has already been used.');
        $isUnique = $this->locations->get_location_by_name($location_name);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $loc_id) {
                return true;
            } else {
                return false;
            }
        }
    }

}
