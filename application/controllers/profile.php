<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
    }

    public function index() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $session_id = $this->session->userdata('user_id');

        if (!$session_id) {
            redirect('login');
        }

        $data = $this->users->get_user_by_id($session_id);
        $this->load->vars($data[0]);
        $this->template->set_layout('default');
        $this->template->title('Profile');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/profile.js") . '"></script>');
        $this->template->build('pages/profile');
    }

    public function validate_fields() {
        $rules = array(
            array(
                'field' => 'employee-id',
                'label' => 'Employee ID',
                'rules' => 'trim|required|callback_check_empId'
            ),
            array(
                'field' => 'first-name',
                'label' => 'First Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'last-name',
                'label' => 'Last Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email-address',
                'label' => 'Email Address',
                'rules' => 'trim|required'
            )
        );

        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('is_unique', 'This %s has been already used');
        //will return true if validated
        if ($this->form_validation->run() == false) {
            $data = array(
                'status' => 'with-error',
                'employeeid' => form_error('employee-id'),
                'firstname' => form_error('first-name'),
                'lastname' => form_error('last-name'),
                'email' => form_error('email-address')
            );

            echo json_encode($data);
        } else {


            $this->update_user();

            $sessionArray = array(
                'employee_id' => $this->input->get_post('employee-id'),
                'first_name' => $this->input->get_post('first-name'),
                'last_name' => $this->input->get_post('last-name'),
                'email' => $this->input->get_post('email-address'),
            );

            $this->session->set_userdata($sessionArray);


            $data = array(
                'status' => 'success'
            );
            echo json_encode($data);
        }
    }

    public function update_user() {
        $data = array(
            'employee_id' => "'" . $this->input->get_post('employee-id') . "'",
            'first_name' => "'" . $this->input->get_post('first-name') . "'",
            'last_name' => "'" . $this->input->get_post('last-name') . "'",
            'email' => "'" . $this->input->get_post('email-address') . "'"
        );

        $this->users->update_user($data, $this->session->userdata['user_id']);
    }

    public function check_empId() {
        $empId = $this->input->post('employee-id');
        $userId = $this->input->post('id');

        $isUnique = $this->users->get_user_by_empid($empId);
        if (!$isUnique) {
            return true;
        } else {
            if ($isUnique[0]['id'] == $userId) {
                return true;
            } else {
                $this->form_validation->set_message('check_empId', 'The %s has already been used.');
                return false;
            }
        }
    }

}
