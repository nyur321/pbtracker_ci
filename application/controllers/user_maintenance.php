<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_maintenance extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users');
        $this->load->model('userpositions');
        $this->load->model('userroles');
        $this->load->model('roles');
        $this->load->model('userpractices');
        $this->load->model('practices');
        $this->load->model('users');
        $this->load->model('userlocations');
        $this->load->model('positions');
        $this->load->model('mailer');
    }

    public function check_if_authorized() {
        if (!$this->session->userdata('logged')) {
            redirect('login');
        }
        $roles = $this->session->userdata('roles');
        if (
                $this->in_array_r("Super Administrator", $roles) || $this->in_array_r("Practice Owner", $roles) || $this->in_array_r("Office Administrator", $roles)
//                || $this->in_array_r("Biller", $roles)
//                || $this->in_array_r("Clinician", $roles)
        ) {
            
        } else {
            redirect('not_authorized');
        }
    }

    public function index() {

        $this->check_if_authorized();

        $users = array(
            'users' => $this->get_user_data(),
        );

        $this->load->vars($users);
        $this->template->set_layout('default');
        $this->template->title('User Maintenance');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
        $this->template->append_metadata('<script src="' . base_url("js/user-maintenance.js") . '"></script>');
        $this->template->build('pages/user_maintenance');
    }

    public function create_user() {
        redirect('create_user');
    }

    public function get_user_data() {
        $rolenames = $this->session->userdata('roles');

        if ($this->in_array_r('Practice Owner', $rolenames)) {
            //ALL USERS UNDER THE PRACTICE
            $pid = $this->session->userdata('practice_id');
            $users = $this->users->get_users_under_prac($pid);
            $userdata = $this->set_user_array($users);
        } else if ($this->in_array_r('Super Administrator', $rolenames)) {
            //ALL USERS
            $users = $this->users->get_all();
            $userdata = $this->set_user_array($users);

        } else if ($this->in_array_r('Office Administrator', $rolenames)) {
            //USERS IN LOCATION OF ADMIN
            $users = $this->users->get_users_underlocs($this->session->userdata('user_id'));
            $userdata = $this->set_user_array($users);
        }
        return $userdata;
    }

    function set_user_array($users) {
        $adminlocs = $this->userlocations->get_admin_locs($this->session->userdata('user_id'));
        $userroles = $this->session->userdata('roles');
        foreach ($users as $user) {
            $roles = $this->userroles->get_role_by_id($user['id']);
            $pos = $this->userpositions->get_posdata($user['id']);
            $locs = $this->userlocations->get_locdata($user['id']);

            $strRole = '';
            $strLocs = '';
            $strCases = '';
            $strDates = '';
            $termDates = '';

            foreach ($roles as $role) {
                $strRole .= $role['role_name'] . '<br/>';
            }

            foreach ($locs as $loc) {

                if ($this->in_array_r('Super Administrator', $userroles) || $this->in_array_r('Practice Owner', $userroles)) {//if super admin or owner
                    $strLocs .= $loc['location_name'] . '<br/>';
                    $strDates .= date('m/d/Y', strtotime($loc['start_date'])) . '<br/>';
                    if ($this->in_array_r('Clinician', $roles)) {
                        $strCases .= $loc['initial_cases'] . '<br/>';
                    } else {
                        $strCases .= '--' . '<br/>';
                    }
                    $termDates .= date('m/d/Y', strtotime($loc['termination_date'])) . '</br>';
                } else { //if office admin
                    if ($this->in_array_r($loc['location_name'], $adminlocs)) {
                        $strLocs .= $loc['location_name'] . '<br/>';
                        $strDates .= date('m/d/Y', strtotime($loc['start_date'])) . '<br/>';
                        if ($this->in_array_r('Clinician', $roles)) {
                            $strCases .= $loc['initial_cases'] . '<br/>';
                        } else {
                            $strCases .= '--' . '<br/>';
                        }
                        $termDates .= date('m/d/Y', strtotime($loc['termination_date'])) . '</br>';
                    }
                }
               
                
            }

            $userdata[] = array(
                'userId' => $user['id'],
                'empId' => $user['employee_id'],
                'name' => $user['first_name'] . ' ' . $user['last_name'],
                'roles' => $strRole,
                'pos' => $pos[0]['position_name'],
                'loc' => $strLocs,
                'startDate' => $strDates,
                'initialCases' => $strCases,
                'termDate' => $termDates,
                'activated' => $user['activation_status'],
                'status' => $user['employment_status']
//                    
            );
        }

        return $userdata;
    }

    function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }

}
