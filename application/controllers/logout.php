<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    
class Logout extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->model('users');
    }
    
    public function index(){
        /*
        $this->template->set_layout('default');
        $this->template->set_partial('header', 'partials/header');
        $this->template->set_partial('footer', 'partials/footer');
        $this->template->set_partial('sidebar', 'partials/sidebar');
         * 
         */
       
    }
    
    public function logout_redirect(){
        $this->session-> unset_userdata($this->session->all_userdata());
        $this->session->sess_destroy();
        redirect('login','refresh');
    }
}
