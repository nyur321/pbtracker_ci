<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar'] ?>
                    </div>

                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <h2 class="main-title">Profile</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Profile</span></p>
                            <form class="form-horizontal" id="edit-form" role="form">
                                <input type="hidden" class="form-control" name="id" value="<?php echo $id ;?>">
                                <div class="form-group">
                                    <label for="employee-id" class="col-xs-2 control-label">Employee ID</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="employee-id" id="employee-id" placeholder="Employee ID" value="<?php echo $employee_id ;?>">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="first-name" class="col-xs-2 control-label">First Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="first-name" id="first-name" placeholder="First Name" value="<?php echo $first_name ;?>">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last-name" class="col-xs-2 control-label">Last Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="last-name" id="last-name"  placeholder="Last Name" value="<?php echo $last_name ;?>">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="roles" class="col-xs-2 control-label">Roles</label>
                                    <div class="col-xs-3">
                                        <?php foreach($this->session->userdata('roles') as $role): ?>
                                        <li><?php echo $role['role_name'] . "<br>" ?></li>
                                        <?php endforeach; ?>
                                        
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email-address" class="col-xs-2 control-label">Email Address</label>
                                    <div class="col-xs-3">
                                        <input type="email" class="form-control" name="email-address" id="email-address" placeholder="Email Address" value="<?php echo $email ;?>">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-offset-2 col-xs-3">
                                        <button type="button" class="btn btn-default submit-info">Submit</button>
                                        <br><span id="profNotification" style="color:green;"></span>
                                    </div>
                                </div>
                            </form>
                        </div>                 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
