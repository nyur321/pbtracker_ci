<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>
                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Admin Dashboard</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display"></span></p>
                            <div class="dashboard-toolbar admin-toolbar">
                                <!--<button class="btn btn-default pull-left" data-toggle="modal" data-target="#user-list-modal-admin">Print Clinicians Daily Report</button>-->
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                Print Report <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a data-toggle="modal" data-target="#user-list-modal-admin" href="#">Clinicians Daily Report</a></li>

                                                <li><a href="" id="print_aggregates" >Clinician Aggregates</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="btn-group pull-right">
                                            <button type="button" id="nyur-toggle" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                Generate Graph <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#" id="gen-bargraph">Bar Graph</a></li>
                                                <li><a href="" id="gen-linegraph">Line Graph</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <?php if (in_array_ar('Super Administrator', $this->session->userdata('roles'))) { ?>
                                    <!--<a href="" ><button id="admin_print_aggregates" class="btn btn-default pull-right">Print Clinician Aggregates</button></a>-->
                                <?php } else { ?>
                                    <!--<a href="" ><button id="print_aggregates" class="btn btn-default pull-right">Print Clinician Aggregates</button></a>-->
                                <?php } ?>
                            </div>
                            <div class="dashboard-table" style="margin-top:10px">

                                <div class="graph-header" id="mindata" style="height: 35px;
                                     background: none repeat scroll 0% 0% #999;
                                     color: #FFF;
                                     font-weight: bold;
                                     text-align: center;
                                     margin-bottom: 10px;
                                     padding-top: 5px;
                                     border-top-left-radius: 5px;
                                     border-top-right-radius: 5px;">
                                </div>
                                <ul class="nav nav-tabs" id="admin-dashboard-tab">
                                    <li class="active"><a href="#volume" data-toggle="tab">Volume</a></li>
                                    <li><a href="#billing" data-toggle="tab">Billing</a></li>
                                    <li><a href="#unit-ratios" data-toggle="tab">Unit Ratios</a></li>
                                    <li><a href="#visit-ratios" data-toggle="tab">Visit Ratios</a></li>
                                    <li><a href="#case-management-ratios" data-toggle="tab">Case Management Ratios</a></li>
                                    <li><a href="#financial-ratios" data-toggle="tab">Financial Ratios</a></li>
                                    <li><a href="#statting-ratios" data-toggle="tab">Statting Ratios</a></li>
                                </ul>

                                <script>
                                    $(function() {
                                        $('#admin-dashboard-tab a:first').tab('show');
                                    });

                                </script>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane active" id="volume">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table" id="vol" cellspacing="0">
                                            <thead >
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>NEW CASES</th>
                                                    <th>ACTIVE D/C</th>
                                                    <th>PASSIVE D/C</th>
                                                    <th>ACTIVE CASES</th>
                                                    <th>VISITS ATT.</th>
                                                    <th>VISITS CANCELED</th>
                                                    <th>VISITS SCHED.</th>
                                                    <th>UNITS CHARGED</th>
                                                    <th>DAYS WORKED</th>
                                                    <th>HOURS WORKED</th>
                                                    <th>CLINICAL HOURS SCHED.</th>
                                                </tr>
                                            </thead>
                                            <tbody id="volume-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="billing">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table" id="bill" cellspacing="0">
                                            <thead>
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>CHARGES</th>
                                                    <th>COLLECTIONS</th>
                                                    <th>BILLING DAYS</th>
                                                </tr>
                                            </thead>
                                            <tbody id="billing-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="unit-ratios">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table" id="unit" cellspacing="0">
                                            <thead>
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>UNITS / VISIT</th>
                                                    <th>UNITS / CASE</th>
                                                    <th>UNITS / HOUR</th>
                                                    <th>UNITS / 8 HOUR</th>
                                                    <th>CHARGES / UNIT</th>
                                                    <th>COLLECTIONS / UNIT</th>
                                                </tr>
                                            </thead>
                                            <tbody id="unitratios-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="visit-ratios">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table" id="visit" cellspacing="0">
                                            <thead>
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>VISITS / CASE</th>
                                                    <th>VISITS / HOUR</th>
                                                    <th>VISITS / 8 HOUR</th>
                                                    <th>CHARGES / VISIT</th>
                                                    <th>COLLECTIONS / VISIT</th>    
                                                    <th>VISITS CANCELED / VISIT</th>  
                                                </tr>
                                            </thead>
                                            <tbody id="visitratios-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="case-management-ratios">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table" id="cases" cellspacing="0">
                                            <thead>
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>ACTIVE CASES / DAY</th>
                                                    <th>PASSIVE D/C RATE</th>
                                                    <th>ATTENDANCE RATE</th>
                                                    <th>PASSIVE D/C / ACTIVE D/C</th>
                                                </tr>
                                            </thead>
                                            <tbody id="casemanage-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="financial-ratios">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table" id="fin" cellspacing="0">
                                            <thead>
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>CHARGES / HOUR</th>
                                                    <th>COLLECTION / HOUR</th>                                               
                                                </tr>
                                            </thead>
                                            <tbody id="finance-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="tab-pane" id="statting-ratios">
                                    <div class="dashboard-table-content" style="display: block;">
                                        <table class="table table-striped table-bordered admin-table"  id="staff" cellspacing="0">
                                            <thead>
                                                <tr class="table-head">
                                                    <th>CLINICIAN</th>
                                                    <th>UNITS / BILLING DAY</th>
                                                    <th>VISITS / BILLING DAY</th>
                                                    <th>TOTAL HOURS / TOTAL DAYS </th>
                                                    <th>SCHEDULE DENSITY</th>
                                                </tr>
                                            </thead>
                                            <tbody id="staff-body">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--MODAL-->
                <div class="modal fade" id="user-list-modal-admin" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                                <h4 class="modal-title"> <strong>Clinicians</strong></h4>   
                                <label class="checkbox" style="margin-top:10px; margin-bottom: -10px">
                                    <input type="checkbox" value="" class="toggleall" checked="checked">
                                    Select All
                                </label>  
                            </div>

                            <div class="modal-body">


                                <form id="clinician-list-admin" target="_BLANK" method="POST" action="<?php echo base_url(); ?>admin_dashboard/generate_individual" style="overflow:auto; margin-top:-15px; margin-left: -2px">
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary pull-right" style="margin-left: 10px;" id="generate-clinician-reports">Generate Reports</button>
                                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>



    $('#print_aggregates').on('click', function(e) {
        e.preventDefault();
//    $('input#start-date-sidebar').val();
//    $('input#end-date-sidebar').val();

        $.form('<?php echo base_url(); ?>dashboard_reports/generate_aggregatesPDF', {start_date: $('input#start-date-sidebar').val(), end_date: $('input#end-date-sidebar').val(), location_id: $('#location-list-sidebar').val()}).submit();


    });

    $('#gen-bargraph').on('click', function(e) {
        e.preventDefault();
        $.form('<?php echo base_url(); ?>bar_graph', {start_date: $('input#start-date-sidebar').val(), end_date: $('input#end-date-sidebar').val(), location_id: $('#location-list-sidebar').val()}).submit();
    });

    $('#gen-linegraph').on('click', function(e) {
        e.preventDefault();
        $.form('<?php echo base_url(); ?>line_graph', {start_date: $('input#start-date-sidebar').val(), end_date: $('input#end-date-sidebar').val(), location_id: $('#location-list-sidebar').val()}).submit();
    });
    
    $.ajax({
        type: "POST",
        url: "admin_dashboard/load_clinician_names",
        data: 'location=' + $('#location-list-sidebar').val() + '&start-date=' + $('#start-date-sidebar').val() + '&end-date=' + $('#end-date-sidebar').val(),
        dataType: 'json',
        success: function(data) {

            $('#clinician-list-admin').html(data.clinician);
            
        }
        

    });



    function enable_button(enable_butt) {
        if (!enable_butt || $('#location-list-sidebar').val() == '0') {
            $('#nyur-toggle').addClass('disabled');
        } else {
            $('#nyur-toggle').removeClass('disabled');
        }
    }

</script>