<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Send Invite</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Send Invites</span></p>

                            <form class="form-horizontal" id="invite-form" role="form">
                                <div class="form-group">
                                    <label for="fname" class="col-xs-2 control-label">First Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="first-name" id="first-name" placeholder="First Name">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lname" class="col-xs-2 control-label">Last Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="last-name" id="last-name" placeholder="Last Name">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-xs-2 control-label">Email Address</label>
                                    <div class="col-xs-3">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="confirm-email" class="col-xs-2 control-label">Confirm Email Address</label>
                                    <div class="col-xs-3">
                                        <input type="email" class="form-control" name="confirmemail" id="confirm-email" placeholder="Confirm Email">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="practice-name" class="col-xs-2 control-label">Practice Name</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="practice-name" id="practice-name" placeholder="Practice Name">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="practice-code" class="col-xs-2 control-label">Practice Code</label>
                                    <div class="col-xs-3">
                                        <input type="text" class="form-control" name="practice-code" id="practice-code" placeholder="Practice Code">
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="position" class="col-xs-2 control-label">Position</label>
                                    <div class="col-xs-4">
                                        <select name="position" class="form-control" id="position">
                                            <?php
                                            foreach ($positions as $pos) {
                                                echo '<option value="' . $pos['id'] . '">' . $pos['position_name'] . ' (' . $pos['position_code'] . ')</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-offset-2 col-xs-3">
                                        <button type="button" id="submit-invite" class="btn btn-default" data-loading-text="Saving...">Submit</button>
                                    </div>
                                </div>


                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
