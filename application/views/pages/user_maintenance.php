<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url()?>img/pb-logo-2.jpg">
                            </div>
                             <h2 class="main-title">User Maintenance</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Users</span></p>
                            <div class="dashboard-toolbar admin-toolbar">
                                <h4 class="pull-left">Active Users</h4>
                                <?php if (in_array_r('Super Administrator', $this->session->userdata('roles'))) { ?>
                                    <button class = "btn btn-default pull-right" onclick = "window.window.location.href = '<?php echo site_url(); ?>admin_create_user'">Create User</button>
                                <?php }else{  ?>
                                      <button class = "btn btn-default pull-right" onclick = "window.window.location.href = '<?php echo site_url(); ?>user_maintenance/create_user'">Create User</button>
                                <?php } ?>
                            </div>
                            <div class="dashboard-table-content">
                                <table class="table table-striped table-bordered" cellspacing="0" id="user-maintenance-table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Emp ID</th>
                                            <th>Name</th>
                                            <th>Roles</th>
                                            <th>Position</th>
                                            <th>Location</th>
                                            <th>Initial Cases</th>
                                            <th>Date of First Use</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($users as $user) {
                                            if ($user['activated'] != 2 && $user['userId'] != 1) {
                                                if ($user['status'] == 1) {//if user is active
                                                    if ($user['activated'] == 1) {//if account is activated 
                                                        echo '<tr>';
                                                    } else {
                                                        echo '<tr style="color:red !important;">';
                                                    }
                                                    ?>

                                                <td><?php echo $user['empId'] ?></td>
                                                <td><?php echo $user['name'] ?></td>
                                                <td><?php echo $user['roles'] ?></td>
                                                <td><?php echo $user['pos'] ?></td>
                                                <td><?php echo $user['loc'] ?></td>
                                                <td><?php echo $user['initialCases'] ?></td>
                                                <td><?php echo $user['startDate'] ?></td>
                                                <td>
                                                    <a userid="<?php echo $user['userId']; ?>" href="" class="edit-user">
                                                        <img src="<?php echo base_url(); ?>img/pencil.png">
                                                    </a>
                                                </td>

                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>
                            <div class="dashboard-toolbar admin-toolbar">
                                <h4 class="pull-left">Terminated Users</h4>
                            </div>
                            <div class="dashboard-table-content">
                                <table class="table table-striped table-bordered" cellspacing="0" id="user-termination-table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Emp ID</th>
                                            <th>Name</th>
                                            <th>Roles</th>
                                            <th>Position</th>
                                            <th>Location</th>
                                            <th>Date of First Use</th>
                                            <th>Termination Date</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($users as $user) {
                                            if ($user['activated'] != 2) {
                                                if ($user['status'] == 0) {//if user is active
                                                    echo '<tr>';
                                                    ?>

                                                <td><?php echo $user['empId'] ?></td>
                                                <td><?php echo $user['name'] ?></td>
                                                <td><?php echo $user['roles'] ?></td>
                                                <td><?php echo $user['pos'] ?></td>
                                                <td><?php echo $user['loc'] ?></td>
                                                <td><?php echo $user['startDate'] ?></td>
                                                <td><?php echo $user['termDate'] ?></td>
                                                <td>
                                                    <a userid="<?php echo $user['userId']; ?>" href="" class="edit-user">
                                                        <img src="<?php echo base_url(); ?>img/pencil.png">
                                                    </a>
                                                </td>

                                                </tr>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>

                            <?php
                            if (in_array_r('Super Administrator', $this->session->userdata('roles'))) {
                                ?>
                                <div class="dashboard-toolbar admin-toolbar">
                                    <h4 class="pull-left">Pending Users</h4>
                                </div>
                                <div class="dashboard-table-content">
                                    <table class="table table-striped table-bordered" cellspacing="0" id="user-pending-table">
                                        <thead>
                                            <tr class="table-head">
                                                <th>Emp ID</th>
                                                <th>Name</th>
                                                <th>Roles</th>
                                                <th>Position</th>
                                                <th>Location</th>
                                                <th>Initial Cases</th>
                                                <th>Date of First Use</th>
                                                <th>Edit</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 11px;">
                                            <?php
                                            foreach ($users as $user) {
                                                if ($user['activated'] == 2) {

                                                    echo '<tr>';
                                                    ?>

                                                <td><?php
                                        if ($user['empId'] != '') {
                                            echo $user['empId'];
                                        } else {
                                            echo '--';
                                        }
                                                    ?></td>
                                                <td><?php echo $user['name'] ?></td>
                                                <td><?php
                                        if ($user['roles'] != '') {
                                            echo $user['roles'];
                                        } else {
                                            echo '--';
                                        }
                                                    ?></td>
                                                <td><?php echo $user['pos'] ?></td>
                                                <td><?php
                                        if ($user['loc'] != '') {
                                            echo $user['loc'];
                                        } else {
                                            echo '--';
                                        }
                                                    ?></td>
                                                <td><?php
                                        if ($user['initialCases'] != '') {
                                            echo $user['initialCases'];
                                        } else {
                                            echo '--';
                                        }
                                                    ?></td>
                                                <td><?php
                                        if ($user['startDate'] != '') {
                                            echo $user['startDate'];
                                        } else {
                                            echo '--';
                                        }
                                                    ?></td>
                                                <td>
                                                    <a userid="<?php echo $user['userId']; ?>" href="" class="edit-user">
                                                        <img src="<?php echo base_url(); ?>img/pencil.png">
                                                    </a>
                                                </td>

                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>

                                        </tbody>
                                    </table>
                                </div>

                                <?php
                            }

                            function in_array_r($needle, $haystack, $strict = false) {
                                foreach ($haystack as $item) {
                                    if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                                        return true;
                                    }
                                }
                                return false;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>


    $('.edit-user').click(function(e) {
        e.preventDefault();
        var userid = $(this).attr("userid");
        

        $.ajax({
            url: 'edit_user/pass_userid',
            async: false,
            type: 'POST',
            data: "&user-id=" + userid,
            dataType: 'json',
            
            success: function(data) {
                if (data.status == 'success') {
                    window.location.href = "<?php echo site_url('edit_user') ?>";
                }
            }

        });

    });
$('#user-maintenance-table').dataTable({
});
$('#user-termination-table').dataTable({
});
$('#user-pending-table').dataTable({
});



</script>