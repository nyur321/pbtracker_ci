<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url()?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Create Role</h2>
                            <p class="breadcrumbs">Role Maintenance >> <span class="location-display">Create Role</span></p>
                            <form class="form-horizontal" id="create-role" role="form">
                                <h4>Role Information</h4>
                                <div class="form-group">
                                    <label for="fname" class="col-sm-2 control-label">Role Name</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="role-name" id="role-name" placeholder="Role Name">
                                        <span class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="main-office" class="col-sm-4 control-label">Role Type</label>
                                    <div class="col-sm-6">
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" name="role-type" id="role1" value="1" checked>
                                                Clinician
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" name="role-type" id="role2" value="0">
                                                Non-Clinician
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="main-office" class="col-sm-4 control-label">Field Entries</label>
                                        <div class="col-sm-3">

                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="new_cases">New Cases
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="active_discharges">Active Discharges
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="passive_discharges">Passive Discharges
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="visits_attended">Visits Attended
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="no_show">Visits Cancels & No Show
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="units_charged">Units Charged
                                                </label>
                                            </div>

                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="hours_worked">Hours Worked
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="hours_scheduled">Clinical Schedules
                                                </label>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input  name="role-item" class="role-item" type="checkbox" value="charges">Charges
                                                </label>
                                            </div>
                                        </div>
                                    </div>




                                    <button type="button" id="submit-location-edited" class="btn btn-default" style="margin-bottom: 10px">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



