<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar']; ?>
                    </div>
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <h2 class="main-title">Aide Dashboard</h2>
                            <p class="breadcrumbs">Dashboard >> Location One</span></p>
                            <div class="dashboard-table">
                                <div class="graph-header" id="mindata" style="height: 35px;
                                     background: none repeat scroll 0% 0% #999;
                                     color: #FFF;
                                     font-weight: bold;
                                     text-align: center;
                                     margin-bottom: 10px;
                                     padding-top: 5px;
                                     border-top-left-radius: 5px;
                                     border-top-right-radius: 5px;">
                                    June 02, 2014 - June 08, 2014
                                </div>

                                <script>
                                    $(function() {
                                        $('#admin-dashboard-tab a:first').tab('show');
                                    });

                                </script>
                            </div>
                                
                            <div style="heigt:auto;">
                            <label vertical-align: middle" class="pull-left clear-right">Select User: </label>
                              <select style="width:250px;" name="location-list-sidebar" id='location-list-sidebar' class="form-control location-selector">
                                  <option value="Glenn Victor Cabatbat">Glenn Esguerra (GP3)</option>
                                  <option value="Glenn Victor Cabatbat">Jerick Paolo Fernandez</option>
                                  
                              </select>
                            </div>
                        
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>