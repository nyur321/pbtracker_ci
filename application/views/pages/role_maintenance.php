<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url()?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Role Maintenance</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display">Roles</span></p>









                            <h4 class="pull-left">Default Roles</h4>
                            <div class="dashboard-toolbar admin-toolbar">
                                <button class="btn btn-default pull-right" onclick="window.window.location.href = '<?php echo site_url(); ?>create_role'">Create Custom Role</button>
                            </div>
                            <div class="dashboard-table-content">
                                <table class="table table-striped table-bordered" style="text-align:left !important; "  id="role-maintenance-table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Role Name</th>
                                            <th>Role Type</th>
                                            <th>Fields</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Practice Owner</td>
                                            <td>Clinician</td>
                                            <td style="text-align:left !important; ">N/A</td>
                                        </tr>
                                        <tr>
                                            <td>Office Administrator</td>
                                            <td>Non-Clinician</td>
                                            <td style="text-align:left !important; ">N/A</td>
                                        </tr>
                                        <tr>
                                            <td>Clinician</td>
                                            <td>Clinician</td>
                                            <td style="text-align:left !important; ">
                                                <ul>
                                                    <li>New Cases</li>
                                                    <li>Active Discharges</li>
                                                    <li>Passive Discharges</li>
                                                    <li>Visits Attended</li>
                                                    <li>Visits Cancels</li>
                                                    <li>Units Charged</li>
                                                    <li style="color:red">Hours Worked</li>
                                                    <li>Clinical Hours Scheduled</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Biller</td>
                                            <td>Non-Clinician</td>
                                            <td style="text-align:left !important;">
                                                <ul>
                                                    <li>Charges</li>
                                                </ul>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>








                                <h4 class="pull-left">Custom Roles</h4>
                                <div class="dashboard-toolbar admin-toolbar">

                                </div>

                                <table class="table table-striped table-bordered" style="text-align:left !important; "  id="role-maintenance-table">
                                    <thead>
                                        <tr class="table-head">
                                            <th>Role Name</th>
                                            <th>Role Type</th>
                                            <th style="column-width: 30px">Fields</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr>
                                            <td>Aide</td>
                                            <td>Non-Clinician</td>
                                            <td style="column-width: 30px;text-align:left !important; ">
                                                <ul >
                                                    <li>Hours Worked</li>

                                                </ul></td>
                                            <td style="text-align:center;">
                                                <a  href="#"  >
                                                    <img src="<?php echo base_url(); ?>img/pencil.png">
                                                </a>
                                            </td>

                                        </tr>


                                    </tbody>
                                </table>





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

