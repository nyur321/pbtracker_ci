
<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="registration-box">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="registration-form">
                            <form class="form-horizontal" id="form-owner" role="form">
                                <div class="page1" style="display:none;">

                                    <!-- datatable Ids -->

                                    <div class="form-group">
                                        <!--                                        <label for="password" class="col-sm-4 control-label">Authentication Key</label>-->
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" name="auth-key"  id="auth-key" placeholder=""
                                                   value="<?php echo $valCode; ?>"> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <!--                                        <label for="password" class="col-sm-4 control-label">Authentication Key</label>-->
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" name="user-id"  id="user-id" placeholder=""
                                                   value="<?php echo $userId; ?>"> 
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <!--                                        <label for="password" class="col-sm-4 control-label">Authentication Key</label>-->
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" name="practice-id"  id="practice-id" placeholder=""
                                                   value="<?php echo $practiceID; ?>"> 
                                        </div>
                                    </div>

                                    <!--end-->

                                    <div class="form-group">
                                        <label for="password" class="col-sm-4 control-label">Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                            <label id="strength" style="font-size:12px"></label>
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmpassword" class="col-sm-4 control-label">Confirm Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm password">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="page2">
                                    <div class="form-group">
                                        <label for="emp-id" class="col-sm-4 control-label">Employee ID</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="emp-id" id="emp-id" placeholder="Employee ID"
                                                   value="<?php echo $empId; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-fname" class="col-sm-4 control-label">First Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="reg-fname" id="reg-fname" placeholder="First Name"
                                                   value="<?php echo $firstName; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-lname" class="col-sm-4 control-label">Last Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="reg-lname" id="reg-lname" placeholder="Last Name"
                                                   value="<?php echo $lastName; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-email" class="col-sm-4 control-label">Email Address</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" name="reg-email" id="reg-email" placeholder="Email Address"
                                                   value="<?php echo $email; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="page3">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="practice-name" class="col-sm-4 control-label">Practice Name</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" name="practice-name" id="practice-name" placeholder="Practice Name"
                                                       value="<?php echo $practiceName; ?>">
                                                <span class="error"></span>
                                            </div>
                                        </div>
                                        <label for="practice-code" class="col-sm-4 control-label">Practice Code</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="practice-code" id="practice-code" placeholder="Practice Code"
                                                   value="<?php echo $practiceCode; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="location-code" class="col-sm-4 control-label">Location Code</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="location-code" id="location-code" placeholder="Location Code">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="location-name" class="col-sm-4 control-label">Location Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="location-name" id="location-name" placeholder="Location Name">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="collection-rate" class="col-sm-4 control-label">Collection Rate</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="collection-rate" id="collection-rate" placeholder="Collection Rate">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reportingmode" class="col-sm-4 control-label">Reporting Mode</label>
                                        <div class="col-sm-8">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="repmode" id="office1" value="0" checked>
                                                    Daily
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="repmode" id="office2" value="1">
                                                    Weekly
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="start-date" class="col-sm-4 control-label">Start Date</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="start-date" id="start-date" placeholder="Start Date">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address1" class="col-sm-4 control-label">Address 1</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="address1" id="address1" placeholder="">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="address2" class="col-sm-4 control-label">Address 2</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="address2" id="address2" placeholder="">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="col-sm-4 control-label">City</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="city" id="city" placeholder="">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="city" class="col-sm-4 control-label">State</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="state" id="state">
                                                <option value="" selected="selected">Select a State</option>
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option> 
                                                <option value="AZ">Arizona</option> 
                                                <option value="AR">Arkansas</option> 
                                                <option value="CA">California</option> 
                                                <option value="CO">Colorado</option> 
                                                <option value="CT">Connecticut</option> 
                                                <option value="DE">Delaware</option> 
                                                <option value="DC">District Of Columbia</option> 
                                                <option value="FL">Florida</option> 
                                                <option value="GA">Georgia</option> 
                                                <option value="HI">Hawaii</option> 
                                                <option value="ID">Idaho</option> 
                                                <option value="IL">Illinois</option> 
                                                <option value="IN">Indiana</option> 
                                                <option value="IA">Iowa</option> 
                                                <option value="KS">Kansas</option> 
                                                <option value="KY">Kentucky</option> 
                                                <option value="LA">Louisiana</option> 
                                                <option value="ME">Maine</option> 
                                                <option value="MD">Maryland</option> 
                                                <option value="MA">Massachusetts</option> 
                                                <option value="MI">Michigan</option> 
                                                <option value="MN">Minnesota</option> 
                                                <option value="MS">Mississippi</option> 
                                                <option value="MO">Missouri</option> 
                                                <option value="MT">Montana</option> 
                                                <option value="NE">Nebraska</option> 
                                                <option value="NV">Nevada</option> 
                                                <option value="NH">New Hampshire</option> 
                                                <option value="NJ">New Jersey</option> 
                                                <option value="NM">New Mexico</option> 
                                                <option value="NY">New York</option> 
                                                <option value="NC">North Carolina</option> 
                                                <option value="ND">North Dakota</option> 
                                                <option value="OH">Ohio</option> 
                                                <option value="OK">Oklahoma</option> 
                                                <option value="OR">Oregon</option> 
                                                <option value="PA">Pennsylvania</option> 
                                                <option value="RI">Rhode Island</option> 
                                                <option value="SC">South Carolina</option> 
                                                <option value="SD">South Dakota</option> 
                                                <option value="TN">Tennessee</option> 
                                                <option value="TX">Texas</option> 
                                                <option value="UT">Utah</option> 
                                                <option value="VT">Vermont</option> 
                                                <option value="VA">Virginia</option> 
                                                <option value="WA">Washington</option> 
                                                <option value="WV">West Virginia</option> 
                                                <option value="WI">Wisconsin</option> 
                                                <option value="WY">Wyoming</option>
                                            </select>
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="zipcode" class="col-sm-4 control-label">Zip Code</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Zip Code">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="office" class="col-sm-4 control-label">Main Office</label>
                                        <div class="col-sm-8">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="office" id="office1" value="1" checked>
                                                    Yes
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="office" id="office2" value="0">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="page4">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="finish-registration">
                                                <h3>Thank You!</h3>
                                                <p>You have successfully created your account</p>
                                                <p>Please proceed to the <a href="<?php echo base_url(); ?>">login page</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="registration-buttons">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-default pull-left prev-butt"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Previous</button>
                                    <button class="btn btn-default pull-right next-butt">Next&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="registration-navi">
                            <div class="registration-pane">
                                <div class="pane-cover pc1">
                                    <div class="pane-number">
                                        1
                                    </div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        Create your password
                                    </div>
                                </div>
                            </div>
                            <div class="registration-pane">
                                <div class="pane-cover pc2">
                                    <div class="pane-number">2</div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        Complete Practice<br>Owner's Information
                                    </div>
                                </div>
                            </div>
                            <div class="registration-pane">
                                <div class="pane-cover pc3">
                                    <div class="pane-number">
                                        3
                                    </div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        Add your <br> Primary Location
                                    </div>

                                </div>
                            </div>
                            <div class="registration-pane">
                                <div class="pane-cover pc4">
                                    <div class="pane-number">
                                        4
                                    </div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        Finish
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
