<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="registration-box">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="registration-form">
                            <form class="form-horizontal" id="form-owner" role="form">
                                <div class="page1" style="display:none;">
                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <input type="hidden" class="form-control" name="auth-key"  id="auth-key" placeholder=""
                                                   value="<?php echo $valCode; ?>">
                                            <input type="hidden" class="form-control" name="user-id"  id="user-id" placeholder=""
                                                   value="<?php echo $userId; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="password" class="col-sm-4 control-label">Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                            <label id="strength" style="font-size:12px"></label>
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="confirmpassword" class="col-sm-4 control-label">Confirm Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Confirm password">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="page2">
                                    <div class="form-group">
                                        <label for="emp-id" class="col-sm-4 control-label">Employee ID</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="emp-id" id="emp-id" placeholder="Employee ID"
                                                   value="<?php echo $empId; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-fname" class="col-sm-4 control-label">First Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="reg-fname" id="reg-fname" placeholder="First Name"
                                                   value="<?php echo $fName; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-lname" class="col-sm-4 control-label">Last Name</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="reg-lname" id="reg-lname" placeholder="Last Name"
                                                   value="<?php echo $lName; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-email" class="col-sm-4 control-label">Email Address</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" name="reg-email" id="reg-email" placeholder="Email Address"
                                                   value="<?php echo $email; ?>">
                                            <span class="error"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="page3">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="finish-registration">
                                                <h3>Thank You!</h3>
                                                <p>You have successfully created your account</p>
                                                <p>Please proceed to the <a href="<?php echo base_url(); ?>">login page</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="registration-buttons">
                            <div class="row">
                                <div class="col-sm-12">
                                    <button class="btn btn-default pull-left prev-butt"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Previous</button>
                                    <button class="btn btn-default pull-right next-butt">Next&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="registration-navi">
                            <div class="registration-pane">
                                <div class="pane-cover pc1">
                                    <div class="pane-number">
                                        1
                                    </div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        Create your password
                                    </div>
                                </div>
                            </div>
                            <div class="registration-pane">
                                <div class="pane-cover pc2">
                                    <div class="pane-number">2</div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        User's Information
                                    </div>
                                </div>
                            </div>
                            <div class="registration-pane">
                                <div class="pane-cover pc3">
                                    <div class="pane-number">3</div>
                                    <div class="pane-check">
                                        <div class="glyphicon glyphicon-ok"></div>
                                    </div>
                                    <div class="panetext">
                                        Finish
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

