<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>
                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar'] ?>
                    </div>

                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Data Field Maintenance</h2>
                            <p class="breadcrumbs">Dashboard >> Data Field Maintenance <span class="location-display"></span></p>

                            <div style="margin-top:20px;">
                                <label for="location" class="col-sm-1 control-label">Location</label>
                                <div class="col-sm-3">
                                    <select name="location" class="form-control" id="location">
                                        <option>Wausau</option>

                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>

                            <button class = "btn btn-default pull-right" data-toggle="modal" href="#" data-target="#create-role-modal">Create Role</button>
                        </div>
                        <div class="col-xs-11 reset-padding">
                            <div style="margin-left:50px; margin-right:auto;margin-top: 20px; width:940px">
                                <table class="table table-striped table-bordered" cellspacing="0">
                                    <thead >
                                        <tr class="table-head">
                                            <th>ROLE</th>
                                            <th>NEW CASES</th>
                                            <th>ACTIVE D/C</th>
                                            <th>PASSIVE D/C</th>
                                            <th>ACTIVE CASES</th>
                                            <th>VISITS ATT.</th>
                                            <th>VISITS CANCELED</th>
                                            <th>VISITS SCHED.</th>
                                            <th>UNITS CHARGED</th>
                                            <th>DAYS WORKED</th>
                                            <th>HOURS WORKED</th>
                                            <th>CLINICAL HOURS SCHED.</th>
                                            <th>CHARGES</th>
                                            <th style="width:40px"> .    ACTION________  </th>
                                        </tr>
                                    </thead>
                                    <tbody id="">
                                        <tr style="text-align:center">
                                            <td  style="text-align:left">Clinician</td>

                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td> 
                                            <td><input type="checkbox"/></i></td>

<!--                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-ok"></i></td> 
                                            <td><i class="glyphicon glyphicon-remove"></i></td>-->
                                            <td style="width:40px"> 
                                                <a  title="Edit" userid=""  data-toggle="modal" href="#" data-target="#edit-role-modal">
                                                    <img src="<?php echo base_url(); ?>img/pencil.png">
                                                </a>

                                                <a  title="Cancel" userid=""  data-toggle="modal" href="#" data-target="#edit-role-modal">
                                                    <i class="glyphicon glyphicon-remove-circle" style="font-size:20px;color:#666666;"></i>
                                                </a>

                                                <a  title="Save" userid=""  data-toggle="modal" href="#" data-target="#edit-role-modal">
                                                    <i class="glyphicon glyphicon glyphicon-ok-circle" style="font-size:20px;color:#0066cc"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr style="text-align:center">
                                            <td  style="text-align:left">Biller</td>
                                            <td><i class="glyphicon glyphicon-remove"></td>    
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-remove"></td>   
                                            <td><i class="glyphicon glyphicon-remove"></td>   
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-remove"></td>   
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-remove"></td>
                                            <td><i class="glyphicon glyphicon-ok"></i></td>
                                            <td style="width:40px"> <a userid=""  data-toggle="modal" href="#" data-target="#edit-role-modal">
                                                    <img src="<?php echo base_url(); ?>img/pencil.png">
                                                </a></td>
                                        </tr>

<!--                                          <tr style="text-align:center">
    <td style="text-align:left">Biller 2</td>
    <td><i class="glyphicon glyphicon-remove"></td>    
    <td><i class="glyphicon glyphicon-remove"></td>
    <td><i class="glyphicon glyphicon-remove"></td>
    <td><i class="glyphicon glyphicon-remove"></td>   
    <td><i class="glyphicon glyphicon-remove"></td>   
    <td><i class="glyphicon glyphicon-remove"></td>
    <td><i class="glyphicon glyphicon-remove"></td>   
    <td><i class="glyphicon glyphicon-remove"></td>
    <td><i class="glyphicon glyphicon-remove"></td>
    <td><i class="glyphicon glyphicon-ok"></td>
    <td><i class="glyphicon glyphicon-remove"></td>
    <td><i class="glyphicon glyphicon-ok"></i></td>
    <td> <a userid=""  data-toggle="modal" href="#" data-target="#edit-role-modal">
            <img src="<?php echo base_url(); ?>img/pencil.png">
        </a></td>
</tr>-->

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>


        <div class="modal fade" id="create-role-modal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" >&times;</button>
                        <h4 class="modal-title"> <strong>Create Role</strong></h4>   
                    </div>

                    <div class="modal-body">


                        <form id="clinician-list" style="overflow:auto; margin-top:-15px; margin-left: -2px">
                            <div class="form-group">
                                <label for="lname" class="col-sm-3 control-label" style="text-align:right">Role Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="role-name" id="first-name" placeholder="Role Name" value="" >
                                    <span class="error"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lname" class="col-sm-3 control-label" style="text-align:right"  >Role Fields</label>
                                <div class="col-sm-9">
                                    <div class="checkbox col-sm-9">
                                        <label>New Cases
                                            <input  name="new-cases" class="role-item" type="checkbox" value="New Cases"/>
                                        </label>
                                    </div>

                                    <div class="checkbox col-sm-9">
                                        <label>Active Discharges
                                            <input  name="active-discharges" class="role-item" type="checkbox" value="Active Discharges"/>
                                        </label>
                                    </div>

                                    <div class="checkbox col-sm-9">
                                        <label>Passive Discharges
                                            <input  name="passive-discharges" class="role-item" type="checkbox" value="Passive Discharges"/>
                                        </label>

                                    </div>
                                    <div class="checkbox col-sm-9">

                                        <label>Visits Attended
                                            <input  name="visits-attended" class="role-item" type="checkbox" value="Visits Attended"/>
                                        </label> 
                                    </div>
                                    <div class="checkbox col-sm-9">
                                        <label>Visits Canceled
                                            <input  name="visits-canceled" class="role-item" type="checkbox" value="Visits Canceled"/>
                                        </label>

                                    </div>
                                    <div class="checkbox col-sm-9">

                                        <label>Units Charged
                                            <input  name="units-charged" class="role-item" type="checkbox" value="Units Charged"/>
                                        </label>
                                    </div>   
                                    <div class="checkbox col-sm-9">
                                        <label>Hours Worked
                                            <input  name="hours-worked" class="role-item" type="checkbox" value="Hours Worked"/>
                                        </label>
                                    </div>
                                    <div class="checkbox col-sm-9">
                                        <label>Clinical Hours Scheduled
                                            <input  name="clinical-hours-scheduled" class="role-item" type="checkbox" value="Clinical Hours Scheduled"/>
                                        </label>

                                    </div>

                                    <div class="checkbox col-sm-9">
                                        <label>Charges
                                            <input  name="charges" class="role-item" type="checkbox" value="Charges"/>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="update-graph">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="edit-role-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title"> <strong>Edit Role</strong></h4>   
                </div>

                <div class="modal-body">


                    <form id="clinician-list" style="overflow:auto; margin-top:-15px; margin-left: -2px">
                        <div class="form-group">
                            <label for="lname" class="col-sm-3 control-label" style="text-align:right">Role Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="role-name" id="first-name" placeholder="Role Name" value="" >
                                <span class="error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lname" class="col-sm-3 control-label" style="text-align:right"  >Role Fields</label>
                            <div class="col-sm-9">
                                <div class="checkbox col-sm-9">
                                    <label>New Cases
                                        <input  name="new-cases" class="role-item" type="checkbox" value="New Cases"/>
                                    </label>
                                </div>

                                <div class="checkbox col-sm-9">
                                    <label>Active Discharges
                                        <input  name="active-discharges" class="role-item" type="checkbox" value="Active Discharges"/>
                                    </label>
                                </div>

                                <div class="checkbox col-sm-9">
                                    <label>Passive Discharges
                                        <input  name="passive-discharges" class="role-item" type="checkbox" value="Passive Discharges"/>
                                    </label>

                                </div>
                                <div class="checkbox col-sm-9">

                                    <label>Visits Attended
                                        <input  name="visits-attended" class="role-item" type="checkbox" value="Visits Attended"/>
                                    </label> 
                                </div>
                                <div class="checkbox col-sm-9">
                                    <label>Visits Canceled
                                        <input  name="visits-canceled" class="role-item" type="checkbox" value="Visits Canceled"/>
                                    </label>

                                </div>
                                <div class="checkbox col-sm-9">

                                    <label>Units Charged
                                        <input  name="units-charged" class="role-item" type="checkbox" value="Units Charged"/>
                                    </label>
                                </div>   
                                <div class="checkbox col-sm-9">
                                    <label>Hours Worked
                                        <input  name="hours-worked" class="role-item" type="checkbox" value="Hours Worked"/>
                                    </label>
                                </div>
                                <div class="checkbox col-sm-9">
                                    <label>Clinical Hours Scheduled
                                        <input  name="clinical-hours-scheduled" class="role-item" type="checkbox" value="Clinical Hours Scheduled"/>
                                    </label>

                                </div>

                                <div class="checkbox col-sm-9">
                                    <label>Charges
                                        <input  name="charges" class="role-item" type="checkbox" value="Charges"/>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="update-graph">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>


