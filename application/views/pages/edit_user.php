<?php ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-sm-2">
                        <?php echo $template['partials']['sidebar']; ?>
                    </div>
                    <div class="col-sm-10 reset-padding">
                        <div class="dashboard-body">
                            <h2 class="main-title">Edit User</h2>
                            <p class="breadcrumbs">User Maintenance >> <span class="location-display">Edit</span></p>
                            <form class="form-horizontal" id="invite-form" role="form">
                                <h4>Basic Information:</h4>

                                <div class="form-group">
                                    <label for="fname" class="col-sm-2 control-label">Employee ID</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" class="form-control" name="user-id" id="first-name" value="<?php echo $userData[0]['id'] ?>">
                                        <input type="text" class="form-control" name="emp-id" id="first-name" placeholder="Employee ID" value="<?php echo $userData[0]['employee_id'] ?>" >
                                        <span class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="lname" class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="first-name" id="first-name" placeholder="Last Name" value="<?php echo $userData[0]['first_name'] ?>" >
                                        <span class="error"></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="lname" class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" name="last-name" id="last-name" placeholder="Last Name" value="<?php echo $userData[0]['last_name'] ?>" >
                                        <span class="error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Roles</label>
                                    <div class="col-sm-3">
                                        <?php
                                        foreach ($roleOptions as $role) {
                                            if ($role['id'] > 1) {
                                                if (($role['id'] == 2 && in_array_r('Practice Owner', $this->session->userdata('roles'))) || ($role['id'] == 3 && in_array_r('Practice Owner', $this->session->userdata('roles'))) || (in_array_r('Super Administrator', $this->session->userdata('roles')))) {
                                                    if (in_array_r($role['role_name'], $userRoles)) {
                                                        echo '<div class="checkbox">
                                                        <label>
                                                            <input  name="roles[]" checked class="role-item" type="checkbox" value="' . $role['id'] . '">
                                                            ' . $role['role_name'] . '
                                                        </label>
                                                    </div>';
                                                    } else {
                                                        echo '<div class="checkbox">
                                                        <label>
                                                            <input  name="roles[]"  class="role-item" type="checkbox" value="' . $role['id'] . '">
                                                            ' . $role['role_name'] . '
                                                        </label>
                                                    </div>';
                                                    }
                                                } else if ($role['id'] > 3) {
                                                    if (in_array_r($role['role_name'], $userRoles)) {
                                                        echo '<div class="checkbox">
                                                        <label>
                                                            <input  name="roles[]" checked class="role-item" type="checkbox" value="' . $role['id'] . '">
                                                            ' . $role['role_name'] . '
                                                        </label>
                                                    </div>';
                                                    } else {
                                                        echo '<div class="checkbox">
                                                        <label>
                                                            <input  name="roles[]"  class="role-item" type="checkbox" value="' . $role['id'] . '">
                                                            ' . $role['role_name'] . '
                                                        </label>
                                                    </div>';
                                                    }
                                                }
                                            }
                                        }

                                        function in_array_r($needle, $haystack, $strict = false) {
                                            foreach ($haystack as $item) {
                                                if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                                                    return true;
                                                }
                                            }
                                            return false;
                                        }
                                        ?>

                                        <span class="error"  id="role-error"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="position-code" class="col-sm-2 control-label">Position Code</label>
                                    <div class="col-sm-3">
                                        <select name="position" class="form-control" id="position">
                                            <?php
                                            foreach ($positionOptions as $pos) {
                                                if ($userPosition[0]['position_id'] == $pos['id']) {
                                                    echo '<option value="' . $pos['id'] . '" selected>' . $pos['position_name'] . ' (' . $pos['position_code'] . ')</option>';
                                                } else {
                                                    echo '<option value="' . $pos['id'] . '">' . $pos['position_name'] . ' (' . $pos['position_code'] . ')</option>';
                                                }
                                            }
                                            ?>
                                        </select>

                                        <span class="error"></span>
                                    </div>
                                    <a href ="" id="add-position" data-toggle="modal" data-target="#add-pos-modal">Add Position</a>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email Address</label>
                                    <div class="col-sm-3">
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $userData[0]['email'] ?>">
                                        <span class="error"></span>
                                    </div>
                                </div>

                                <h4>User's Locations:</h4>
                                <div id="user-locations">
                                    <?php echo $locMarkup ?>         
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-3">
                                        <span class="error" id="noloc"></span>
                                        <button type="button"  id="add-location" data-toggle="modal" data-target="#add-loc-modal" class="btn btn-default">Add a location</button>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-4">

                                        <?php
                                        if (in_array_r('Super Administrator', $this->session->userdata('roles'))) {
                                            ?>      
                                            <?php if ($userData[0]['activation_status'] == 2) { ?>
                                                <button type="button"  id="save-admin" class="btn btn-default" data-loading-text="Saving...">Submit</button>
                                            <?php } else { ?>
                                                <button type="button"  id="save" class="btn btn-default" data-loading-text="Saving...">Submit</button>
                                            <?php } ?>
                                            <?php if ($userData[0]['activation_status'] == 2) { ?>
                                                <button type="button"  id="send-invite-admin" class="btn btn-default" data-loading-text="Sending Invitation...">Send Invite</button>
                                            <?php } ?>
                                            <?php if ($userData[0]['activation_status'] == 0) { ?>
                                                <button type="button"  id="resend-invite" class="btn btn-default" data-loading-text="Saving...">Resend Invite</button>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <button type="button"  id="save" class="btn btn-default" data-loading-text="Saving...">Submit</button>
                                            <?php if ($userData[0]['activation_status'] == 0) { ?>
                                                <button type="button"  id="resend-invite" class="btn btn-default" data-loading-text="Sending Invitation...">Resend Invite</button>
                                            <?php } ?>


                                        <?php } ?>

                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!--MODAL for ADD POSITION-->
    <div class="modal fade" id="add-pos-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title"> <strong>Add Position </strong></h4>
                </div>

                <div class="modal-body">
                    <form id="add-pos-form">
                        <div class="form-group">
                            <label for="position-code">Position Code</label>
                            <input type="text" name="position-code" class="form-control " id="position-code" placeholder="Position Code">
                            <span class="error"></span>
                        </div>


                        <div class="form-group">
                            <label for="position-name">Position Name</label>
                            <input type="text" name="position-name" class="form-control " id="position-name" placeholder="Position Name">
                            <span class="error"></span>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="add-pos-btn">Add</button>
                </div>
            </div>
        </div>
    </div>


    <!--MODAL for ADD LOCATION-->

    <div class="modal fade" id="add-loc-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                    <h4 class="modal-title"> <strong>Add Location </strong></h4>
                </div>
                <div class="modal-body">

                    <form id="add-loc-form">
                        <div class="form-group">
                            <label for="location">Location</label>
                            <select name="location-id" class="form-control" id="location-id">
                                <?php
                                echo $locOptions;
                                ?>
                            </select>
                            <span class="error"></span>
                        </div>
                        <div class="form-group">
                            <label for="date-first-use">Date of First Use</label>
                            <div class="">
                                <input type="text" name="date-first-use" class="form-control first-date" id="date-first-use" placeholder="">
                                <span class="error"></span>
                            </div>
                        </div>

                        <div class="form-group initial-cases">
                            <label class="control-label" >Initial Cases</label>
                            <div class="">
                                <input type="text" class="form-control" value="0" name="initial-cases" id="initial-cases" placeholder="">
                                <span class="error"></span>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" id="add-loc-btn">Add</button>
                </div>
            </div>
        </div>
    </div>

    <script>

        hide_button(<?php echo $countlocs; ?>);
        $(".initial-cases").hide();//hide initial cases first
        $('.first-date').datepicker();
        function hide_button(bool) {
            if (bool == 1) {
                $('#add-location').hide();

            } else {
                $('#add-location').show();
            }
        }

        function update_locs(data) {
            $('#location-id').empty();
            $('#location-id').append(data);
        }

        $(".role-item[value='4']").click(function() {
            if ($(this).is(":checked")) {
                $(".initial-cases").show();

            } else {
                $(".initial-cases").hide();

            }
        });

        initial_cases_check();

        function initial_cases_check() {
            if ($(".role-item[value='4']").is(":checked")) {
                $(".initial-cases").show();
            } else {
                $(".initial-cases").hide();
            }
        }

        $('#add-loc-btn').click(function(e) {
            e.preventDefault();

            $.ajax({
                url: 'edit_user/add_location',
                async: false,
                type: 'POST',
                data: $('#add-loc-form').serialize() + "&location-name=" + $('#location-id option:selected').text(),
                dataType: 'json',
                success: function(data) {
                    if (data.status == 'success') {
                        var div = document.createElement('div');
                        div.id = 'location_' + data.id;
                        div.innerHTML = data.markup;
                        document.getElementById('user-locations').appendChild(div);

                        initial_cases_check();

                        update_locs(data.locOptions);
                        hide_button(data.countlocs);
                        $('#noloc').html('');
                        $('#add-loc-modal').modal('hide');
                        $('.first-date').datepicker();
                    } else if (data.status == 'error') {
                        if (data.date !== '') {
                            error('#date-first-use', data.date);
                        } else {
                            reset('#date-first-use');
                        }

                        if (data.initial !== '') {
                            error('#initial-cases', data.initial);
                        } else {
                            reset('#initial-cases');
                        }
                    }
                }
            });
        });


        $("#container .termination-date").datepicker();


        $('.container').on("click", ".remove-loc", function(e) {
            e.preventDefault();
            var locid = $(this).attr("locid");
            $.ajax({
                url: 'edit_user/remove_location',
                async: false,
                type: 'POST',
                data: "&location-id=" + locid,
                dataType: 'json',
                success: function(data) {
                    if (data.status == 'success') {
                        var id = 'location_' + locid;
                        var element = document.getElementById(id);
                        element.parentNode.removeChild(element);

                        update_locs(data.locOptions);
                        hide_button(data.countlocs);
                    }
                }
            });
        });


        $('.container').on('click', '.term[value="1"]', function() {
            var id = $(this).attr('term');

            if ($(this).is(":checked")) {
                $('div#' + id).hide();
            } else {
                $('div#' + id).show();
            }
        });

        $('.container').on('click', '.term[value="0"]', function() {
            var id = $(this).attr('term');

            if ($(this).is(":checked")) {
                $('#' + id).show();
            } else {
                $('#' + id).hide();
            }
        });

        $('#save').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: 'edit_user/validate_entry',
                async: false,
                type: 'POST',
                data: $('#invite-form').serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $('#save').button('loading');
                },
                success: function(data) {
                    reset('input');
                    if (data.status == 'success') {
                        new PNotify({
                            title: 'Success',
                            text: 'The user\'s information has been updated.',
                            type: 'success'
                        });
                    } else if (data.status == 'error') {

                        new PNotify({
                            title: 'Error',
                            text: 'Please check the data you entered.',
                            type: 'error'
                        });

                        if (data.empid !== '') {
                            error('input[name=emp-id]', data.empid);
                        } else {
                            reset('input[name=emp-id]');
                        }

                        if (data.fname !== '') {
                            error('input[name=first-name]', data.fname);
                        } else {
                            reset('input[name=first-name]');
                        }

                        if (data.lname !== '') {
                            error('input[name=last-name]', data.lname);
                        } else {
                            reset('input[name=last-name]');
                        }

                        if (data.email !== '') {
                            error('input[name=email]', data.email);
                        } else {
                            reset('input[name=email]');
                        }

                        if (data.role !== '') {
                            $('#role-error').html('Please select a Role.');
                        } else {
                            $('#role-error').html('');
                        }

                        if (data.noloc == 'noloc') {
                            $('#noloc').html('Please add at least 1 Location.');
                        } else {
                            $('#noloc').html('');
                        }

                        if (data.valdate != '') {
                            $.each(data.valdate, function(key, value) {
                                error('.' + value, 'Date of First Use is required.');
                            });
                        }

                        if (data.valcase != '') {
                            $.each(data.valcase, function(key, value) {
                                error('.' + value, 'Initial Active Case is required.');
                            });
                        }

                        if (data.termdate != '') {
                            $.each(data.termdate, function(key, value) {
                                error('.' + value, 'Termination Date is required.');
                            });
                        }
                    }


                    $('#save').button('reset');
                }

            });
        });

        $('#save-admin').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: 'edit_user/save_user_admin',
                async: false,
                type: 'POST',
                data: $('#invite-form').serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $('#save-admin').button('loading');
                },
                success: function(data) {
                    reset('input');
                    $('#save-admin').button('reset');
                    if (data.status == 'success') {
                        new PNotify({
                            title: 'Success',
                            text: 'The user\'s information has been updated.',
                            type: 'success'
                        });
                    } else if (data.status == 'error') {

                        new PNotify({
                            title: 'Error',
                            text: 'Please check the data you entered.',
                            type: 'error'
                        });
                        if (data.fname != '') {
                            error('input[name=first-name]', data.fname);
                        } else {
                            reset('input[name=first-name]');
                        }
                        if (data.lname != '') {
                            error('input[name=last-name]', data.lname);
                        } else {
                            reset('input[name=last-name]');
                        }
                        if (data.email != '') {
                            error('input[name=email]', data.email);
                        } else {
                            reset('input[name=email]');
                        }
                        if (data.empid != '') {
                            error('input[name=emp-id]', data.empid);
                        } else {
                            reset('input[name=empid]');
                        }

                        if (data.valdate != '') {
                            $.each(data.valdate, function(key, value) {
                                error('.' + value, 'Date of First Use is required.');
                            });
                        }

                        if (data.valcase != '') {
                            $.each(data.valcase, function(key, value) {
                                error('.' + value, 'Initial Active Case is required.');
                            });
                        }

                        if (data.termdate != '') {
                            $.each(data.termdate, function(key, value) {
                                error('.' + value, 'Termination Date is required.');
                            });
                        }
                    }
                }

            });
        });
        $('#send-invite-admin').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: 'edit_user/save_user_invite',
                async: false,
                type: 'POST',
                data: $('#invite-form').serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $('#send-invite-admin').button('loading');
                },
                success: function(data) {
                    reset('input');
                    if (data.status == 'success') {


                        new PNotify({
                            title: 'Success',
                            text: 'The user\'s information has been updated.',
                            type: 'success'
                        });
                        new PNotify({
                            title: 'Invitation Sent',
                        });
                    } else if (data.status == 'error') {
                        if (data.empid !== '') {
                            error('input[name=emp-id]', data.empid);
                        } else {
                            reset('input[name=emp-id]');
                        }

                        if (data.fname !== '') {
                            error('input[name=first-name]', data.fname);
                        } else {
                            reset('input[name=first-name]');
                        }

                        if (data.lname !== '') {
                            error('input[name=last-name]', data.lname);
                        } else {
                            reset('input[name=last-name]');
                        }

                        if (data.email !== '') {
                            error('input[name=email]', data.email);
                        } else {
                            reset('input[name=email]');
                        }

                        if (data.role !== '') {
                            $('#role-error').html('Please select a Role.');
                        } else {
                            $('#role-error').html('');
                        }

                        if (data.noloc == 'noloc') {
                            $('#noloc').html('Please add at least 1 Location.');
                        } else {
                            $('#noloc').html('');
                        }

                        if (data.valdate != '') {
                            $.each(data.valdate, function(key, value) {
                                error('.' + value, 'Date of First Use is required.');
                            });
                        }

                        if (data.valcase != '') {
                            $.each(data.valcase, function(key, value) {
                                error('.' + value, 'Initial Active Case is required.');
                            });
                        }

                        if (data.termdate != '') {
                            $.each(data.termdate, function(key, value) {
                                error('.' + value, 'Termination Date is required.');
                            });
                        }

                        new PNotify({
                            title: 'Error',
                            text: 'Please check the data you entered.',
                            type: 'error'
                        });
                    }
                    $('#send-invite-admin').button('reset');
                }
            });
        });
        $('#resend-invite').click(function(e) {
            e.preventDefault();
            $.ajax({
                url: 'edit_user/resend_invite',
                async: false,
                type: 'POST',
                data: $('#invite-form').serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $('#resend-invite').button('loading');
                },
                success: function(data) {
                    $('#resend-invite').button('reset');
                    if (data.status == 'success') {
                        new PNotify({
                            title: 'Invitation Sent',
                        });
                    } else {
                        new PNotify({
                            title: 'Error',
                            text: 'The invitation was not sent. Please contact your adminisitrator',
                            type: 'error'
                        });
                    }
                }
            });
        });

        $('#add-pos-btn').click(function(e) {
            e.preventDefault();

            $.ajax({
                url: 'create_user/create_pos',
                async: false,
                type: 'POST',
                data: $('#add-pos-form').serialize(),
                dataType: 'json',
                success: function(data) {
                    if (data.status == 'error') {
                        if (data.pcode !== '') {
                            $('input[name=position-code]').parent().addClass('has-error');
                            $('input[name=position-code]').siblings('span').html(data.pcode);
                        } else {
                            $('input[name=position-code]').parent().removeClass('has-error');
                            $('input[name=position-code]').siblings('span').html('');
                        }

                        if (data.pname !== '') {
                            $('input[name=position-name]').parent().addClass('has-error');
                            $('input[name=position-name]').siblings('span').html(data.pname);
                        } else {
                            $('input[name=position-name]').parent().removeClass('has-error');
                            $('input[name=position-name]').siblings('span').html('');
                        }
                        new PNotify({
                            title: 'Error',
                            text: 'Please check the data you entered.',
                            type: 'error'
                        });

                    } else if (data.status == 'success') {

                        new PNotify({
                            title: 'Success',
                            text: 'The role has been created.',
                            type: 'success'
                        });

                        $('#position').html(data.posmarkup);


                        $('#add-pos-modal').modal('hide');
                        $('input[name=position-code]').parent().removeClass('has-error');
                        $('input[name=position-code]').siblings('span').html('');
                        $('input[name=position-code]').val('');


                        $('input[name=position-name]').parent().removeClass('has-error');
                        $('input[name=position-name]').siblings('span').html('');
                        $('input[name=position-name]').val('');
                    }
                }
            });
        });
    </script>