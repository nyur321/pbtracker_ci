<div class="container">
    <div class="row">
        <div class="col-md-12 reset-padding">
            <div class="inner-content">
                <div class="inner-content-header">
                    <div>Performance Tracker</div>
                </div>

                <div class="row">
                    <div class="col-xs-2">
                        <?php echo $template['partials']['sidebar']; ?>

                    </div>
                    <div class="col-xs-10 reset-padding">
                        <div class="dashboard-body">
                            <div class="pblogo pull-right">
                                <img src="<?php echo base_url() ?>img/pb-logo-2.jpg">
                            </div>
                            <h2 class="main-title">Minimum Data Set Dashboard</h2>
                            <p class="breadcrumbs">Dashboard >> <span class="location-display" ></span></p>
                            <div class="dashboard-toolbar minimum-dataset-toolbar">
                                <button class="btn btn-default pull-left" data-toggle="modal" data-target="#minimum-dataset-modal">Generate Report</button>
                            </div>
                            <div class="dashboard-table-content">
                                <div id="mindata" style="height: 35px;
                                     background: none repeat scroll 0% 0% #999;
                                     color: #FFF;
                                     font-weight: bold;
                                     text-align: center;
                                     margin-bottom: 10px;
                                     padding-top: 5px;
                                     border-top-left-radius: 5px;
                                     border-top-right-radius: 5px;" >

                                </div>

                                <table class="table table-striped table-bordered" id="minimum-maintenance-table">
                                    <thead> 
                                        <tr class="" style="background:#BBB;">
                                            <th rowspan="2" colspan="1" style="text-align:center" >CLINICIAN </th>
                                            <th colspan="3" style="text-align: center" >UTILIZATION</th>
                                            <th colspan="4"  style="text-align: center" >PRODUCTIVITY</th>
                                            <th colspan="2"  style="text-align: center" >SATISFACTION</th>
                                        </tr>
                                        <tr class="table-head">
                                            <th >VISITS / CASE</th>
                                            <th >UNITS / VISIT</th>
                                            <th >ESTIMATED COLLECTED REVENUE</th>
                                            <th >SCHEDULED HOURS / WORKED HOUR</th>
                                            <th >UNITS / WORKED HOUR</th>
                                            <th >VISITS / WORKED HOUR</th>
                                            <th >CHARGES / WORKED HOUR</th>
                                            <th >VISITS CANCELED / VISITS ATT.</th>
                                            <th >PASSIVE D/C / ACTIVE D/C</th>
                                        </tr>

                                    </thead>
                                    <tbody> 

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="minimum-dataset-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h4 class="modal-title"> <strong>Clinicians</strong></h4>   
                <label class="checkbox" style="margin-top:10px; margin-bottom: -10px">
                    <input type="checkbox" value="" class="toggleall" checked="checked">
                    Select All
                </label>  
            </div>
            <div class="modal-body">
                <form id="clinician-list-minimum" target="_BLANK" method="POST" action="<?php echo base_url(); ?>minimum_dataset_dashboard/generate_minimum_dataset_report" style="overflow:auto; margin-top:-15px; margin-left: -2px">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" style="margin-left: 10px;" id="generate-minimum-dataset-report">Generate Reports</button>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

