<?php ?>
<html>
    <body style="text-align: center; font-family:Arial, Helvetica, sans-serif; color: gray;">
        <div class="wrapper" style="width: 500px; margin: 0 auto;">
            <div style="border: 2px solid #dedede; border-radius: 10px;">
                <div style="height: 30px; background-image: url(<?php echo $headerBgLink ?>); text-align: center; font-family: Arial, Helvetica, sans-serif; color: gray;">
                    <h3> Performance Tracker </h3>
                </div>
                <div id='email-body' style="text-align: left; margin: 20px; font-family:Arial, Helvetica, sans-serif; color: gray;">
                    <?php echo date('F d, Y'); ?>
                    <img src="<?php echo $pbLogoLink; ?>" style="position: relative; left: 5px; top: -15px; float: right;">
                    <br/>
                    <br/>
                    <br/>
                    <p>Dear <?php echo $firstName . " " . $lastName; ?>,
                        <br/>
                        <br/>
                        With reference to your weekly stats, the following weekly data have not yet been closed for <?php echo $location_name?><br><br>
                        <?php echo date('M d, Y',strtotime($start_date)).' to '.date('M d, Y',strtotime($end_date));?>
                        
                        <br/>
                        <br/>
                        To close your weekly data, you need to login to your account. On the <?php echo $dashboard?>, click on a date on the calendar that is within the date range of the reporting week you want to close. On the lower right of the <?php echo $table_name?>table, click on the link labeled &#34;Close Weekly Data&#34;. Note: Closing the weekly data would cause the fields to be uneditable.  
                        <br/>
                        <br/>
                        <a href='<?php echo $siteurl; ?>'> Go to Tracker </a>
                        <br/>
                        <br/>
                        <br/>
                        Thank you,
                        <br/>
                        <br/>
                        
                    </p>
                </div>
                <div id="email-footer" style="text-align: left; margin: 20px; font-family:Arial, Helvetica, sans-serif; color: gray;">
                    <p style="line-height: 25px;">
                        Performance Tracker Support Team
                        <br/>
                        <a href="mailto:support@prosoft-phils.com">support@prosoft-phils.com</a><br/>
                        404 835 7950
                  </p>
                   
                </div>
            </div>
        </div>
    </body>
</html>