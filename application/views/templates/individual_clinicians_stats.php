<style>
    table,th,td
    {
        border:1px solid black;
        border-collapse:collapse;
        width: 100%;
        font-size: 200px;
    }
</style>

<h6>Employee: Employee Name</h6>
<h6>Location: Location</h6>

<table align="center">
    <thead>
        <tr>
            <th colspan="14">Volume</th>
        </tr>
    
        <tr>
            <th>Date</th>
            <th>New Cases</th>
            <th>Active Discharges</th>
            <th>Passive Discharges</th>
            <th>Active Cases</th>
            <th>Visits Attended</th>
            <th>Visits Cancels & No Shows</th>
            <th>Visits Schedule</th>
            <th>Units Charged</th>
            <th>Hours Worked</th>
            <th>Clinical Hours Scheduled</th>
            <th>Charges</th>
            <th>Estimated Collections</th>
            <th>Collection Rate</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>10/10/10</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
            <th>12</th>
            <th>13</th>
        </tr>
    </tbody>
    
</table>

<h6>Total Billing Days: 3</h6>

<table align="center">
    <thead>
        <tr>
            <th colspan="1"></th>
            <th colspan="6">Unit Ratios</th>
            <th colspan="5">Visit Ratios</th>
        </tr>
        <tr>
            <th>Date</th>
            <th>Units/Visits</th>
            <th>Visits/Active Case</th>
            <th>Units/Hours Worked</th>
            <th>Units/8 Hours Worked</th>
            <th>Charge/Unit</th>
            <th>Collected/Unit</th>
            <th>Visits/Case</th>
            <th>Visits/Hour Worked</th>
            <th>Visits/8 Hours Worked</th>
            <th>Charges/Visit</th>
            <th>Collected/Visit</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>10/10/10</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
        </tr>
    </tbody>
    
</table>
<br/>
<table align="center">
    <thead>
        <tr>
            <th colspan="1"></th>
            <th colspan="3">Case Management Ratios</th>
            <th colspan="2">Financial Ratios</th>
            <th colspan="4">Staffing Ratios</th>
        </tr>
        <tr>
            <th>Date</th>
            <th>Active Cases/Day</th>
            <th>Passive Discharge Rate</th>
            <th>Attendance Rate</th>
            <th>Charge/Hour Worked</th>
            <th>Collected/Hour Worked</th>
            <th>Units/Billing Day</th>
            <th>Visits/Billing Day</th>
            <th>Total Hours Worked/Days Worked</th>
            <th>Schedule Density</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>10/10/10</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
        </tr>
    </tbody>
</table>

