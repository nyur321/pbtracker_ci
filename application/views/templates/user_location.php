<div class="form-group">
    <label class="col-sm-2 control-label">Location</label>
    <div class="col-sm-3">
        <input type="hidden" value="<?php echo $locId ?>" name="location-id[]">
        <input type="text" class="form-control" readonly="read-only" value="<?php echo $locName ?>" >
        <span class="error"></span>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label" >Date of First Use</label>
    <div class="col-sm-3">
        <input type="text" class="form-control first-date date_<?php echo $locId?>" value="<?php echo $startDate ?>" name="date-first-use[]">
        <span class="error"></span>
    </div>
</div>
<div class="form-group initial-cases">
    <label class="col-sm-2 control-label" >Initial Cases</label>
    <div class="col-sm-3">
        <input type="text" class="form-control case_<?php echo $locId?>" value="<?php echo $initialCases ?>" name="initial-cases[]" id="initial-cases" placeholder="">
        <span class="error"></span>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-3">
        <button type="button" id="remove-location"  locid="<?php echo $locId ?>"class="btn btn-default remove-loc">Remove</button>
    </div>
</div>

<hr>




