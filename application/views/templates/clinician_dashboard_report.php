<style>
    
    table,th,td
    {
        border:1px solid black;
        border-collapse:collapse;
        font-size: 24px;
        text-align: center;
    }
    th,td{
        padding:10px;
    }
    
    td{
        text-align: right;
    }
    
    #totals{
        text-align: center;
    }
    
</style>


<h6>Employee: <?php echo $user_name;?></h6>

<h6>Location: <?php echo $location_practice[0]['location_name'];?></h6>

<table align="center" class="" style="">
    <thead>
        <tr>
            <th>Date</th>
            <th>New Cases</th>
            <th>Active Discharges</th>
            <th>Passive Discharges</th>
            <th>Active Cases</th>
            <th>Visits Attended</th>
            <th>Visits Cancels & No Shows</th>
            <th>Visits Schedule</th>
            <th>Units Charged</th>
            <th>Hours Worked</th>
            <th>Clinical Hours Scheduled</th>
            <th>Charges</th>
            <th>Estimated Collections</th>
            <th>Collection Rate</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($clinician_data as $cli_data){?>
        <tr>
            <td width="7%"><?php echo date('m/d/Y',  strtotime($cli_data['start_date']));?></td>
            <td><?php echo $cli_data['new_cases'];?></td>
            <td><?php echo $cli_data['active_discharges'];?></td>
            <td><?php echo $cli_data['passive_discharges'];?></td>
            <td><?php echo $cli_data['active_cases'];?></td>
            <td><?php echo $cli_data['visits_attended'];?></td>
            <td><?php echo $cli_data['no_show'];?></td>
            <td><?php echo $cli_data['visits_scheduled'];?></td>
            <td><?php echo $cli_data['units_charged'];?></td>
            <td><?php echo $cli_data['hours_worked'];?></td>
            <td><?php echo $cli_data['hours_scheduled'];?></td>
            <td>$ <?php echo number_format($cli_data['charges'],2);?></td>
            <td>$ <?php echo number_format($cli_data['estimated_collections'],2);?></td>
            <td><?php echo $cli_data['collection_rate'];?> %</td>
        </tr>
        
        <?php $billing_total += $cli_data['billing_days']; 
              $new_cases_total += $cli_data['new_cases'];
              $active_discharges_total += $cli_data['active_discharges'];
              $passive_discharges_total += $cli_data['passive_discharges'];
              $active_cases_total = $cli_data['active_cases'];
              $visits_attended_total += $cli_data['visits_attended'];
              $no_show_total += $cli_data['no_show'];
              $visits_scheduled_total += $cli_data['visits_scheduled'];
              $units_charged_total += $cli_data['units_charged'];
              $hours_worked_total += $cli_data['hours_worked'];
              $hours_scheduled_total += $cli_data['hours_scheduled'];
              $charges_total += $cli_data['charges'];
              $estimated_collections_total += $cli_data['estimated_collections'];
              $collection_rate_total += $cli_data['collection_rate'];
        
        } ?>
        <tr>
            <td id="totals">Totals</td>
            <td><?php echo $new_cases_total;?></td>
            <td><?php echo $active_discharges_total;?></td>
            <td><?php echo $passive_discharges_total;?></td>
            <td><?php echo $active_cases_total;?></td>
            <td><?php echo $visits_attended_total;?></td>
            <td><?php echo $no_show_total;?></td>
            <td><?php echo $visits_scheduled_total;?></td>
            <td><?php echo $units_charged_total;?></td>
            <td><?php echo $hours_worked_total;?></td>
            <td><?php echo $hours_scheduled_total;?></td>
            <td>$ <?php echo number_format($charges_total,2);?></td>
            <td>$ <?php echo $estimated_collections_total;?></td>
        </tr>
    </tbody>
    
</table>

<p><?php // print_r($clinician_data);?></p>

<h6>Total Billing Days: <?php echo $billing_total;?></h6>
