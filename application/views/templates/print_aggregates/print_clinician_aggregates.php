<style>
    table{
        border:1px solid black;
        width:100%;
    }
    td{
        border:1px solid black;
    }
    tr{
        border:1px solid black;
    }
    tr th{
        border:1px solid black;
    }
    
</style>

<h6>Locations: Location</h6>

<table align="center">
    <thead>
        <tr>
            <th colspan="12">Volume</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>New Cases</th>
            <th>Active D/C</th>
            <th>Passive D/C</th>
            <th>Active Cases</th>
            <th>Visits Att.</th>
            <th>Visits Cancels</th>
            <th>Visits Schedule</th>
            <th>Units Charged</th>
            <th>Days Worked</th>
            <th>Hour Worked</th>
            <th>Clinical Hours Sched.</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
            <th>10</th>
            <th>11</th>
        </tr>
    </tbody>
    
</table>
<br/>
<table align="center">
    <thead>
        <tr>
            <th colspan="4">Billing</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Charges</th>
            <th>Estimated Collections</th>
            <th>Billing Days</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
        </tr>
    </tbody>
</table>
<br/>
<table align="center">
    <thead>
        <tr>
            <th colspan="7">Unit Ratios</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Units/Visit</th>
            <th>Units/Active Case</th>
            <th>Units/Hour Worked</th>
            <th>Units/8 Hours Worked</th>
            <th>Charge/Unit</th>
            <th>Collected/Unit</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
        </tr>
    </tbody>
</table>
<br/>
<table align="center">
    <thead>
        <tr>
            <th colspan="6">Visit Ratios</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Visits/Case</th>
            <th>Visits/Hour Worked</th>
            <th>Visits/8 Hours Worked</th>
            <th>Charges/Visit</th>
            <th>Collected/Visit</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
        </tr>
    </tbody>
</table>

<br/>
<table align="center">
    <thead>
        <tr>
            <th colspan="4">Case Management Ratios</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Active Cases/Day</th>
            <th>Passive Discharge Rate</th>
            <th>Attendance Rate</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
        </tr>
    </tbody>
    
</table>
<br/>
<table align="center">
    <thead>
        <tr>
            <th colspan="3">Financial Ratios</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Charge/Hour Worked</th>
            <th>Collected/Hour Worked</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
        </tr>
    </tbody>
</table>
<br/>   
<table align="center">
    <thead>
        <tr>
            <th colspan="5">Staffing Ratios</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Units/Billing Day</th>
            <th>Visits/Billing Day</th>
            <th>Total Hours Worked/Days Worked</th>
            <th>Schedule Density</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Aran the Great</td>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
        </tr>
    </tbody>
    
</table>