ung <?php ?>
<html>
    <body style="text-align: center; font-family:Arial, Helvetica, sans-serif; color: gray;">
        <div class="wrapper" style="width: 500px; margin: 0 auto;">
            <div style="border: 2px solid #dedede; border-radius: 10px;">
                <div style="height: 30px; background-image: url(<?php echo $headerBgLink ?>); text-align: center; font-family: Arial, Helvetica, sans-serif; color: gray;">
                    <h3> PERFORMANCE TRACKER </h3>
                </div>

                <div id='email-body' style="text-align: left; margin: 20px; font-family:Arial, Helvetica, sans-serif; color: gray;">
                    <?php echo date('F d, Y'); ?>
                    <img src="<?php echo $pbLogoLink; ?>" style="position: relative; left: 5px; top: -15px; float: right;">
                    <br/>
                    <br/>
                    <br/>
                    <p style="font-family:Arial, Helvetica, sans-serif; color: gray;">Dear <?php echo $firstName . " " . $lastName; ?>,
                        <br/>
                        <br/>
                        Please use the temporary password provided below to log into your account.
                        <br/>
                        <br/>
                        Password: <span style="font-size: 16px;"><strong><?php echo $password ?></strong></span>
                        <br/>
                        <br/>
                        Once you are logged into your account, you will be asked to change your password.
                        <br/>
                        <br/>
                        <br/>
                        Thank you,
                        <br/>
                    </p>
                </div>

                <div id="email-footer" style="text-align: left; margin: 20px; margin-bottom:15px; font-family:Arial, Helvetica, sans-serif; color: gray;">
                    <p>
                        Performance Tracker Admin
                        <br/> </p>
                    <p style="font-style: italic;">support@prosoft-phils.com</p> <br/>
					<p style="font-style: italic;">404.835.7950 / 815.315.9585</p>
                </div>
            </div>
        </div>
    </body>
</html>