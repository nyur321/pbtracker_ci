<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Performance Tracker | <?php echo $template['title']; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>fonts/font-face.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/pnotify.custom.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
        <script>window.jQuery || document.write('<script src="<?php echo base_url(); ?>js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="<?php echo base_url(); ?>js/vendor/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/highcharts.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/exporting.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/jquery.dataTables.js"></script>     
        <script src="<?php echo base_url(); ?>js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/dataTables.bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>js/vendor/pnotify.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>js/main.js"></script>
    </head>
    <body>

        <?php echo $template['body'] ?>

        <?php echo $template['partials']['footer']; ?>

        <?php echo $template['metadata']; ?>
       
    </body>
</html>
