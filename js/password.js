function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i = 0; i < pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 70)
        return 4;

    if (score > 40)
        return 3;

    if (score >= 20)
        return 2;

    if (score >= 10)
        return 1;

    return "";
}


$('#password').on('input', function() {
    var strength = checkPassStrength(document.getElementById('password').value);
    var color = "#FF0000";
    var str = "Weak";
    switch (strength) {
        case 1:
            var color = "#FF0000";
            var str = "Weak";
            break;
        case 2:
            var color = "#FF9900";
            var str = "Average";
            break;
        case 3:
            var color = "#999900";
            var str = "Good";
            break;
        case 4:
            var color = "#00FF00";
            var str = "Strong";
            break;
    }
    if (document.getElementById('password').value == "") {
         document.getElementById("strength").innerHTML = "";
    } else {
        document.getElementById("strength").innerHTML = "Password Strength: " + "<span style='font-weight:bold; color:" + color + ";'>" + str + "</span>";
    }
});