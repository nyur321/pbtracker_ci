$('#submit-location-edited').on('click', function(e) {
    e.preventDefault();
    $.ajax({
        async: false,
        type: 'POST',
        url: 'create_role/submit_role',
        data: $('#create-role').serialize(),
        dataType: 'json',
        beforeSend: function() {

        },
        success: function(data) {
            console.log('nyur');

            if (data.error == '1') {
                if (data.role_name !== '') {
                    error('input[name=role-name]', data.role_name);
                } else {
                    reset('input[name=role-name]');
                }
            } else {
                refresh_all_fields();
            }

        }

    });
});

function error(element, html) {
    $(element).parent().parent().addClass('has-error');
    $(element).siblings('span').html(html);
}

function reset(element) {
    $(element).parent().parent().removeClass('has-error');
    $(element).siblings('span').html('');
}

function refresh_all_fields() {
    $('input').parent().parent().removeClass('has-error');
    $('input').siblings('span').html('');
}

function empty_fileds() {
    $('input').val('');

}