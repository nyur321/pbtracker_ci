
$('#submit-invite').on('click', function(e) {
    e.preventDefault();
    $.ajax({
        async: false,
        url: 'invite/entry_validation',
        data: $('form#invite-form').serialize(),
        dataType: 'json',
        type: 'post',
        beforeSend: function() {
             var x = 0;
            setInterval(function () {
                if(x<5){
                x += 1;
                }
                if(x === 1){
                    $('#submit-invite').html('Submitting...');
                }else if(x === 5){
                    $('#submit-invite').html('Submit');
                }  
                
                },100);

        },
        success: function(data) {
            if (data.status === 'error1') {
                    new PNotify({
                    title:'Error',
                    text: 'Please check the data you entered.',
                    type: 'error'
                });
                if (data.firstname !== '') {
                    error('input[name=first-name]', data.firstname);
                } else {
                    reset('input[name=first-name]');
                }
                if (data.lastname !== '') {
                    error('input[name=last-name]', data.lastname);
                } else {
                    reset('input[name=last-name]');
                }
                if (data.email !== '') {
                    error('input[name=email]', data.email);
                } else {
                    reset('input[name=email]');
                }
                if (data.confirmemail !== '') {
                    error('#confirm-email', data.confirmemail);
                } else {
                    reset('#confirm-email');
                }
                if (data.practicename !== '') {
                    error('input[name=practice-name]', data.practicename);
                } else {
                    reset('input[name=practice-name]');
                }
                if (data.practicecode !== '') {
                    error('input[name=practice-code]', data.practicecode);
                } else {
                    reset('input[name=practice-code]');
                }

            } else {
                refresh_all_fields();
                empty_fileds();
                new PNotify({
                    title:'Success',
                    text: 'The user has been created.',
                    type: 'success'
                });
                  new PNotify({
                    title: 'Invitation Sent',
                });
            }

          
        }
    });
});



function error(element, html) {
    $(element).parent().parent().addClass('has-error');
    $(element).siblings('span').html(html);
}

function reset(element) {
    $(element).parent().parent().removeClass('has-error');
    $(element).siblings('span').html('');
}

function refresh_all_fields() {
    $('input').parent().parent().removeClass('has-error');
    $('input').siblings('span').html('');
}

function empty_fileds() {
    $('input').val('');

}


   