<div style="width:100%; background:#DDDDDD;padding:10px;">
<div style="font-family:arial; font-size:10px; color:#444444; background:#ffffff; width:95%; border-radius:5px; border:1px solid #888888; margin-left:auto; margin-right:auto;">

<div id="emailheader" style="padding-top:40px; padding-bottom:30px">
	<div style="width:inherit;text-align:center;"> <h1> Performance Tracker </h1>
	</div>

</div>

<div style="font-size:15px; padding-bottom:40px; width:80%; margin-left:auto; margin-right:auto">
<!--contents here-->
<p><?php echo date('F d, Y'); ?></p>
<br/>
<br/>
<br/>
<p>Dear <?php echo $firstname." ".$lastname; ?>,</p>
<br/>
<p>You have requested for a new password. Please use the password we provided below to log into your account.</p>
<br/>
<p>PASSWORD: <strong><?php echo $password; ?></strong></p>
<br/>
<p>For your account's security, please change your password once you are logged in.</p>
<br/>
<p>Thank you.</p>

</div>
 
<div style="font-size:15px;padding-bottom:40px; width:80%; margin-left:auto; margin-right:auto">
	<img src="<?php echo base_url();?>/img/pb-logo-2.jpg">
	<!--logo here-->
</div>
</div>

<div style="height:20px;font-family:arial;color:#444444;font-size:11px;width:90%; margin-left:auto; margin-right:auto; margin-top:5px;">
	<div style="float:left;">
	<p>PerformanceBuilders.com </p>
</div>
	<div style="float:right; ">
	<p> &#169; 2014 Performance Builders</p>
</div>
</div>
</div>

